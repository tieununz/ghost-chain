#include "AppDelegate.h"
#include "MainGameScene.h"
#include "MenuScene.h"
#include "AboutScene.h"
#include "ModeScene.h"
#include "SelectLevelScreen.h"
#include "AppMacros.h"
#include <vector>
#include <string>
#include "SoundManager.h"
using namespace std;
USING_NS_CC;
AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    CCDirector* pDirector = CCDirector::sharedDirector();
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();

    pDirector->setOpenGLView(pEGLView);
	
	// Set the design resolution

	CCSize devSize = CCEGLView::sharedOpenGLView()->getFrameSize();
	CCSize designSize = CCSizeMake(480,480*devSize.height/devSize.width);
	//if(devSize.width/devSize.height < 0.6f)
		CCEGLView::sharedOpenGLView()->setDesignResolutionSize(480, 800, kResolutionFixedWidth);
	//else
	//	CCEGLView::sharedOpenGLView()->setDesignResolutionSize(480, 800, kResolutionFixedHeight);

    // turn on display FPS
    pDirector->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    //CCScene *pScene = MainGame::scene(kModeGameHard,0);
	CCScene *pScene = MenuScene::scene();

    // run
    pDirector->runWithScene(pScene);
	SoundManager::getInstance()->playBackground("Sounds/bgSong.mp3",true);
	SoundManager::getInstance()->setVolumeFX(1.0f);
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
	SoundManager::getInstance()->pauseBackground();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();
	
    // if you use SimpleAudioEngine, it must resume here
	SoundManager::getInstance()->resumeBackground();
}
