#ifndef __ABOUT_SCENE_H__
#define __ABOUT_SCENE_H__
#include "cocos2d.h"
#include "MenuScene.h"
USING_NS_CC;

class AboutScene : public CCLayer
{
	virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
public:
	~AboutScene(void);
	virtual bool init();
	static CCScene* scene();
	CREATE_FUNC(AboutScene);
};

#endif // __ABOUT_SCENE_H__