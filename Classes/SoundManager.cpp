
#include "SoundManager.h"

static SoundManager *m_pInstance;

SoundManager::SoundManager()
{
}

SoundManager* SoundManager::getInstance()
{
	if (m_pInstance == 0) {
		m_pInstance = new SoundManager();
		m_pInstance->m_bEnableBackground = CCUserDefault::sharedUserDefault()->getBoolForKey("EnableBackground",true);
		m_pInstance->m_bEnableEffect = CCUserDefault::sharedUserDefault()->getBoolForKey("EnableEffect",true);
	}
	return m_pInstance;
}

bool SoundManager::isEnableBackground()
{
	return m_bEnableBackground;
}


bool SoundManager::isEnableEffect()
{
	return m_bEnableEffect;
}

void SoundManager::setEnableBackground(bool b)
{
	m_bEnableBackground = b;
	CCUserDefault::sharedUserDefault()->setBoolForKey("EnableBackground",m_bEnableBackground);
	CCUserDefault::sharedUserDefault()->flush();
}


void SoundManager::setEnableEffect(bool b)
{
	m_bEnableEffect = b;
	CCUserDefault::sharedUserDefault()->setBoolForKey("EnableEffect",m_bEnableEffect);
	CCUserDefault::sharedUserDefault()->flush();
}


void SoundManager::preloadBackgroundMusic(const char* path)
{
	SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str());
}

void SoundManager::preloadEffect(const char* path)
{
	SimpleAudioEngine::sharedEngine()->preloadEffect(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str());
}


void SoundManager::playBackground(const char* path, bool isPlayAgain, bool loop)
{
	if (m_bEnableBackground) {
		if (isPlayingBackground())
		{
			if (isPlayAgain)
			{
				SimpleAudioEngine::sharedEngine()->playBackgroundMusic(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str(), loop);
			}
		}
		else
		{
			SimpleAudioEngine::sharedEngine()->playBackgroundMusic(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str(), loop);
		}
	}
}

void SoundManager::pauseBackground()
{
	if(SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying())
	{
		SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
	}
}

void SoundManager::resumeBackground()
{
	if(m_bEnableBackground)
	{
		SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
	}
}

void SoundManager::stopBackground()
{
	if(SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying())
	{
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(false);
	}
}

void SoundManager::playEffect(const char *path, bool isLoop)
{
	if (m_bEnableEffect) 
	{
		SimpleAudioEngine::sharedEngine()->playEffect(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str(), isLoop);
	}
}

bool SoundManager::isPlayingBackground()
{
	return SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying();
}

void SoundManager::setVolumeBackground(float _value)
{
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(_value);
}

void SoundManager::setVolumeFX(float _value)
{
	SimpleAudioEngine::sharedEngine()->setEffectsVolume(_value);
}