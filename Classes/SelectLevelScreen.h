#ifndef __SELECTLEVEL_SCENE_H__
#define __SELECTLEVEL_SCENE_H__
#include "cocos2d.h"
#include "MainGameScene.h"
#include "GameConfig.h"
USING_NS_CC;

class SelectLevelScreen : public CCLayer
{
	bool m_isMove;
	CCNode *m_selectLayer;
	void moveDone();
	void update(float dt);
	virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
	void selectLevel(CCObject * pSender);
	CCSize visibleSize;
public:
	void menuHandle(CCObject * pSender);
	~SelectLevelScreen(void);
	virtual bool init();
	static CCScene* scene();
	CREATE_FUNC(SelectLevelScreen);
};

#endif // __SELECTLEVEL_SCENE_H__