//
//  Grid.h
//
//
//  Created by Tieunun on 21/11/2013
//

#ifndef __GRID_H__
#define __GRID_H__
#include "Cell.h"
#include "cocos2d.h"
#include <vector>
#include "MainGameScene.h"
#include "GameConfig.h"
USING_NS_CC;
using namespace std;
class Cell;
class Grid : public CCLayer
{
	
	CCAction * m_explosion;

	void createActions();

	void explosionDone(CCNode* pSender);
public:
	//CC_SYNTHESIZE(vector<Cell*>, m_listCell, ListCell);
	//CC_SYNTHESIZE(vector<Cell*>, m_listTouchingCell, ListTouchingCell);
	
	CCArray* m_listCell;
	CCArray* m_listTouchingCell;
	
	CC_SYNTHESIZE(int, m_lastIndexI, LastIndexI);
	CC_SYNTHESIZE(int, m_lastIndexJ, LastIndexJ);
	CC_SYNTHESIZE(int, m_currentTypeCell, CurrentTypeCell);

	CC_SYNTHESIZE(kModeGame, m_modeGame, ModeGame);
	CC_SYNTHESIZE(kGameState, m_stateGame, StateGame);


	~Grid(void);
	Grid(void);
	static Grid * create(kModeGame _modeGame);
	void initGrid(kModeGame _modeGame);
	void update(float dt);

	bool canTouching(int _indexI, int _indexJ);
	bool isListContain(int _indexI, int _indexJ);

	void moveEndTouching();
	void HandleEndTouch();
	void endTouching();
	void CreateNewCell(Cell* _cell);
	void Falling();
	
	virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
};
#endif // __GRID_H__