#include "AboutScene.h"
#include "SoundManager.h"

CCScene* AboutScene::scene()
{
	CCScene *scene = CCScene::create();
	AboutScene *layer = AboutScene::create();
	scene->addChild(layer);
	return scene;
}

AboutScene::~AboutScene(void)
{

}

bool AboutScene::init()
{
	if ( !CCLayer::init() )
	{
		return false;
	}
	this->setTouchEnabled(true);

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	CCSprite* pBG = CCSprite::create("BG_About.png");
	pBG->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	this->addChild(pBG, kBackground);

	return true;
}

void AboutScene::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent)
{
	CCSetIterator k;
	CCTouch* touch;
	CCPoint tap;

	for( k = pTouches->begin(); k != pTouches->end(); k++)
	{
		touch = (CCTouch*) (*k);
		if(touch)
		{
			SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
			CCScene* newScene = CCTransitionMoveInR::create(0.2f, MenuScene::scene());
			CCDirector::sharedDirector()->replaceScene(newScene);
		}
	}
}