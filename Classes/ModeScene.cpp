#include "ModeScene.h"
#include "GameConfig.h"
#include "SoundManager.h"
CCScene* ModeScene::scene()
{
	CCScene *scene = CCScene::create();
	ModeScene *layer = ModeScene::create();
	scene->addChild(layer);
	return scene;
}

ModeScene::~ModeScene(void)
{

}

bool ModeScene::init()
{
	if ( !CCLayer::init() )
	{
		return false;
	}

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	CCSprite* pBG = CCSprite::create("BG_Mode.png");
	pBG->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	this->addChild(pBG, kBackground);

	CCMenuItem *easyButton = CCMenuItemImage::create(
		"Easy_Button.png",
		"Easy_Button.png",
		this,
		menu_selector(ModeScene::easyButtonHandle) );

	CCMenuItem *normalButton = CCMenuItemImage::create(
		"Normal_Button.png",
		"Normal_Button.png",
		this,
		menu_selector(ModeScene::normalButtonHandle) );

	CCMenuItem *levelButton = CCMenuItemImage::create(
		"Level_Button.png",
		"Level_Button.png",
		this,
		menu_selector(ModeScene::levelButtonHandle) );

	CCMenuItem *howToPlayButton = CCMenuItemImage::create(
		"HowToPlay_Button.png",
		"HowToPlay_Button.png",
		this,
		menu_selector(ModeScene::howToPlayButtonHandle) );

	easyButton->setPosition(ccp(227,visibleSize.height - 202 - FactorY));
	normalButton->setPosition(ccp(243,visibleSize.height - 325 - FactorY));
	levelButton->setPosition(ccp(229,visibleSize.height - 483 - FactorY));
	howToPlayButton->setPosition(ccp(191,visibleSize.height - 609 - FactorY));

	m_pMenu = CCMenu::create(easyButton,normalButton,levelButton,howToPlayButton, NULL);
	
	m_pMenu->setPosition(ccp(0, 0));
	this->addChild(m_pMenu, kForeground);

	return true;
}

void ModeScene::easyButtonHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, MainGame::scene(kModeGameEasy,0));
	CCDirector::sharedDirector()->replaceScene(newScene);
}

void ModeScene::normalButtonHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, MainGame::scene(kModeGameHard,0));
	CCDirector::sharedDirector()->replaceScene(newScene);
}

void ModeScene::levelButtonHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, SelectLevelScreen::scene());
	CCDirector::sharedDirector()->replaceScene(newScene);
}

void ModeScene::howToPlayButtonHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, ModeScene::scene());
	CCDirector::sharedDirector()->replaceScene(newScene);
}