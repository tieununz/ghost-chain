//
//  Grid.cpp
//
//
//  Created by Tieunun on 21/11/2013
//

#include "Grid.h"
#include "GameConfig.h"
#include "SoundManager.h"
using namespace CocosDenshion;
Grid::Grid(void)
{
}

Grid::~Grid(void)
{
	
}

Grid * Grid::create(kModeGame _modeGame)
{
	Grid *grid = new Grid();
	if(grid)
	{
		grid->initGrid(_modeGame);
		grid->autorelease();
		return grid;
	}
	CC_SAFE_DELETE(grid);
	return NULL;
}

void Grid::initGrid(kModeGame _modeGame)
{
	m_stateGame = kGameStateRunning;
	m_modeGame = _modeGame;

	m_listCell = new CCArray();
	m_listCell->retain();

	m_listTouchingCell = new CCArray();
	m_listTouchingCell->retain();

	int Arr[COL][ROW];
	for(int i = 0; i < COL; i++)
	{	
		for(int j = 0; j < ROW; j++)
		{
			Arr[i][j] = 1;
		}
	}
	Cell *_cell;
	for(int i = 0;i < COL; i++)
	{		
		for(int j = 0; j < ROW; j++)
		{
			if(Arr[i][j] != 0)
			{
				int type = 0;
				if(m_modeGame == kModeGameEasy)
					type = (int)(CCRANDOM_0_1() * 4) + 1; // type = 1 -> 4
				else 
					type = (int)(CCRANDOM_0_1() * 5) + 1; // type = 1 -> 5
				_cell = Cell::create(this, 8 + i, j, type);
				this->addChild(_cell,kCellground,kGameObjectCell);
				m_listCell->addObject(_cell);
			}
		}
	}

	this->setTouchEnabled(true);
	m_currentTypeCell = kTypeCellZero;
	createActions();
	Falling();
}

void Grid::createActions()
{
	CCAnimation* animation;
	animation = CCAnimation::create();
	CCSpriteFrame * frame;
	
	for(int i = 1; i <= 4; i++) {
		char szName[100] = {0};
		sprintf(szName, "Explosion_%i.png", i);
		frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(szName);
		animation->addSpriteFrame(frame);
	}

	animation->setDelayPerUnit(1 / 8.0f);
	animation->setRestoreOriginalFrame(true);
	m_explosion = CCSequence::create(
		CCAnimate::create(animation),
		CCCallFuncN::create(this, callfuncN_selector(Grid::explosionDone)),
		NULL
		);
	m_explosion->retain();
}

void Grid::explosionDone(CCNode* pSender)
{
	this->getParent()->removeChild(pSender);
}

void Grid::update(float dt)
{

}

void Grid::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent)
{
	CCSetIterator k;
	CCTouch* touch;
	CCPoint tap;

	for( k = pTouches->begin(); k != pTouches->end(); k++)
	{
		touch = (CCTouch*) (*k);
		if(touch)
		{
			tap = touch->getLocation();
			tap.y -= FactorY;
			CCObject* it;
			CCARRAY_FOREACH(m_listCell, it)
			{
				Cell* cell = dynamic_cast<Cell*>(it);
				if (cell != NULL)
				{
					if(cell->boundingBox().containsPoint(tap))
					{
						m_currentTypeCell = cell->getType();
						m_listTouchingCell->addObject(cell);
						m_lastIndexI = cell->getIndexI();
						m_lastIndexJ = cell->getIndexJ();
						cell->changeTouching();
						switch (cell->getType())
						{
						case kTypeCellOne:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type1.wav");
							break;
						case kTypeCellTwo:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type2.wav");
							break;
						case kTypeCellThree:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type3.wav");
							break;
						case kTypeCellFour:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type4.wav");
							break;
						case kTypeCellFive:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type5.wav");
							break;
						case kTypeCellSix:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type6.wav");
							break;
						case kTypeCellSeven:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type7.wav");
							break;
						case kTypeCellEight:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type8.wav");
							break;
						default:
							SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type8.wav");
							break;
						}
					}
				}
			}
		}
	}
}

void Grid::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
	CCSetIterator i;
	CCTouch* touch;
	CCPoint tap;
	
	for( i = pTouches->begin(); i != pTouches->end(); i++)
	{
		touch = (CCTouch*) (*i);

		if(touch)
		{
			tap = touch->getLocation();
			tap.y -= FactorY;
			CCObject* it;
			CCARRAY_FOREACH(m_listCell, it)
			{
				Cell* cell = dynamic_cast<Cell*>(it);
				if (cell != NULL)
				{
					if((m_currentTypeCell == cell->getType() || m_currentTypeCell == kTypeCellZero) 
						&& cell->boundingBox().containsPoint(tap)
						&& canTouching(cell->getIndexI(), cell->getIndexJ()))
					{
						m_currentTypeCell = cell->getType();
						m_lastIndexI = cell->getIndexI();
						m_lastIndexJ = cell->getIndexJ();
						if(!m_listTouchingCell->containsObject(cell))
						{
							m_listTouchingCell->addObject(cell);
							cell->changeTouching();
							switch (cell->getType())
							{
							case kTypeCellOne:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type1.wav");
								break;
							case kTypeCellTwo:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type2.wav");
								break;
							case kTypeCellThree:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type3.wav");
								break;
							case kTypeCellFour:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type4.wav");
								break;
							case kTypeCellFive:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type5.wav");
								break;
							case kTypeCellSix:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type6.wav");
								break;
							case kTypeCellSeven:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type7.wav");
								break;
							case kTypeCellEight:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type8.wav");
								break;
							default:
								SoundManager::getInstance()->playEffect("Sounds/Touch_Type/sfx_Type8.wav");
								break;
							}
						}
					}
				}
			}

		}
	}
}

void Grid::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
	CCSetIterator i;
	CCTouch* touch;
	CCPoint tap;

	if(m_currentTypeCell != kTypeCellZero)
		moveEndTouching();

	for( i = pTouches->begin(); i != pTouches->end(); i++)
	{
		touch = (CCTouch*) (*i);
		if(touch)
		{
			tap = touch->getLocation();
		}
	}
}

void Grid::moveEndTouching()
{
	int size = m_listTouchingCell->count();
	if(size == 1)
	{
		Cell* cell = dynamic_cast<Cell*>(m_listTouchingCell->objectAtIndex(0));
		cell->resetCell();
		m_listTouchingCell->removeAllObjects();
		m_currentTypeCell = kTypeCellZero;
		return;
	}
	if(size == 2)
	{
		Cell* cell0 = dynamic_cast<Cell*>(m_listTouchingCell->objectAtIndex(0));
		Cell* cell1 = dynamic_cast<Cell*>(m_listTouchingCell->objectAtIndex(1));

		cell0->moveEndTouching(cell1->getIndexI(),cell1->getIndexJ());
		cell1->moveEndTouching(cell0->getIndexI(),cell0->getIndexJ());
	}
	else
	{
		CCObject* it;
		CCARRAY_FOREACH(m_listTouchingCell, it)
		{
			Cell* cell = dynamic_cast<Cell*>(it);
			if (cell != NULL)
			{
				cell->moveEndTouching(m_lastIndexI,m_lastIndexJ);
			}
		}
	}
	
	if(size >= 2)
	{
		this->setTouchEnabled(false);
		CCSequence* seq = CCSequence::create(
			CCDelayTime::create(0.5f),
			CCCallFunc::create(this, callfunc_selector(Grid::HandleEndTouch)),
			NULL
			);
		this->runAction(seq);
	}
	if(size >= 3)
	{		
		CCSprite * pExplosion = CCSprite::createWithSpriteFrameName("Explosion_1.png");
		pExplosion->setPosition(ccp(CELLSIZE/2 + m_lastIndexJ * CELLSIZE, CELLSIZE/2 + m_lastIndexI * CELLSIZE + FactorY) + EXTRAPOS);
		pExplosion->runAction(m_explosion);
		this->getParent()->addChild(pExplosion,kExplosion);
	}
}

void Grid::HandleEndTouch()
{
	int size = m_listTouchingCell->count();
	CCPoint pos = ccp(CELLSIZE/2 + m_lastIndexJ * CELLSIZE, CELLSIZE/2 + m_lastIndexI * CELLSIZE + FactorY) + EXTRAPOS;
	MainGame* mainGame = (MainGame*)this->getParent()->getParent();
	int value = 0;
	int valueTime = 0;

	// calculate bonus Point
	if(size == 2)
	{
		value = SUBTRACTPOINT;
		mainGame->addScoreHandle(value,pos);
	}
	else
	{
		if((m_currentTypeCell == kTypeCellOne
			|| m_currentTypeCell == kTypeCellTwo
			|| m_currentTypeCell == kTypeCellThree
			|| m_currentTypeCell == kTypeCellFour
			|| m_currentTypeCell == kTypeCellFive))
		{
			value = BONUSPOINT_COMMON * size;
			mainGame->addScoreHandle(value,pos);
		}
		if(m_currentTypeCell == kTypeCellSix)
		{
			value = BONUSPOINT_SIX * size;
			mainGame->addScoreHandle(value,pos);
		}
		if(m_currentTypeCell == kTypeCellSeven)
		{
			value = BONUSPOINT_SEVEN * size;
			mainGame->addScoreHandle(value,pos);
		}
		if(m_currentTypeCell == kTypeCellEight)
		{
			value = BONUSPOINT_EIGHT * size;
			mainGame->addScoreHandle(value,pos);
		}
	}

	// calculate bonus Time
	if(size >= 3)
	{
		if(m_currentTypeCell == kTypeCellSix)
		{
			valueTime = BONUSTIME_SIX * size;
			mainGame->addTimeHandle(valueTime);
		}
		if(m_currentTypeCell == kTypeCellSeven)
		{
			valueTime = BONUSTIME_SEVEN * size;
			mainGame->addTimeHandle(valueTime);
		}
		if(m_currentTypeCell == kTypeCellEight)
		{
			valueTime = BONUSTIME_EIGHT * size;
			mainGame->addTimeHandle(valueTime);
		}
	}

	// calculate complete level
	if(size >= 3)
	{
		mainGame->addLevelHandle((kTagCell)m_currentTypeCell,size);
	}

	if((m_currentTypeCell == kTypeCellOne
		|| m_currentTypeCell == kTypeCellTwo
		|| m_currentTypeCell == kTypeCellThree
		|| m_currentTypeCell == kTypeCellFour
		|| m_currentTypeCell == kTypeCellFive)
		&& size == 4)
	{
		Cell *_cell = Cell::create(this,m_lastIndexI,m_lastIndexJ, kTypeCellSix);
		this->addChild(_cell,kCellground,kGameObjectCell);
		m_listCell->addObject(_cell);
	}
	
	//Create new cell
	if((m_currentTypeCell == kTypeCellOne
		|| m_currentTypeCell == kTypeCellTwo
		|| m_currentTypeCell == kTypeCellThree
		|| m_currentTypeCell == kTypeCellFour
		|| m_currentTypeCell == kTypeCellFive)
		&& size >= 5)
	{
		Cell *_cell = Cell::create(this,m_lastIndexI,m_lastIndexJ, kTypeCellSeven);
		this->addChild(_cell,kCellground,kGameObjectCell);
		m_listCell->addObject(_cell);
	}

	if((m_currentTypeCell == kTypeCellSix || m_currentTypeCell == kTypeCellSeven)
		&& size >= 3)
	{
		Cell *_cell = Cell::create(this,m_lastIndexI,m_lastIndexJ, kTypeCellEight);
		this->addChild(_cell,kCellground,kGameObjectCell);
		m_listCell->addObject(_cell);
	}
	endTouching();
}

void Grid::endTouching()
{
	int size = m_listTouchingCell->count();
	CCObject* it;
	CCARRAY_FOREACH(m_listTouchingCell, it)
	{
		Cell* cell = dynamic_cast<Cell*>(it);

		if (cell != NULL)
		{
			m_listCell->removeObject(cell,false);
			if(cell->getIndexI() != m_lastIndexI || cell->getIndexJ() != m_lastIndexJ)
			{
				CreateNewCell(cell);
			}
			if(cell->getIndexI() == m_lastIndexI && cell->getIndexJ() == m_lastIndexJ && (size == 3 || size == 2) && 
				(m_currentTypeCell == kTypeCellOne
				|| m_currentTypeCell == kTypeCellTwo
				|| m_currentTypeCell == kTypeCellThree
				|| m_currentTypeCell == kTypeCellFour
				|| m_currentTypeCell == kTypeCellFive))
			{
				CreateNewCell(cell);
			}
			if(cell->getIndexI() == m_lastIndexI && cell->getIndexJ() == m_lastIndexJ && size == 2 && 
				(m_currentTypeCell == kTypeCellSix
				|| m_currentTypeCell == kTypeCellSeven))
			{
				CreateNewCell(cell);
			}
			if(cell->getIndexI() == m_lastIndexI && cell->getIndexJ() == m_lastIndexJ && m_currentTypeCell == kTypeCellEight)
			{
				CreateNewCell(cell);
			}
			this->removeChild(cell);
		}
	}

	m_listTouchingCell->removeAllObjects();
	m_currentTypeCell = kTypeCellZero;
	Falling();
}

void Grid::CreateNewCell(Cell* oldCell)
{
	int tempIndexI = COL;
	int tempIndexJ = oldCell->getIndexJ();

	bool isDone = false;
	while (!isDone)
	{
		isDone = true;
		CCObject* it;
		CCARRAY_FOREACH(m_listCell, it)
		{
			Cell* cell = dynamic_cast<Cell*>(it);
			if (cell != NULL)
			{
				if (tempIndexI == cell->getIndexI() && tempIndexJ == cell->getIndexJ())
				{
					isDone = false;
					tempIndexI++;
				}
			}
		}
	}
	int type = 0;
	if(m_modeGame == kModeGameEasy) 
		type = (int)(CCRANDOM_0_1() * 4) + 1; // type = 1 -> 4
	else 
		type = (int)(CCRANDOM_0_1() * 5) + 1; // type = 1 -> 5
	Cell *_tempCell = Cell::create(this,tempIndexI,tempIndexJ,type);
	this->addChild(_tempCell);
	m_listCell->addObject(_tempCell);
}

void Grid::Falling()
{
	CCObject* it;
	CCARRAY_FOREACH(m_listCell, it)
	{
		Cell* cell = dynamic_cast<Cell*>(it);
		if (cell != NULL)
		{
			int count = 0;
			int _tempI = cell->getIndexI();
			int _tempJ = cell->getIndexJ();
			for (int m = _tempI; m >= 0; m--)
			{
				if (!isListContain(m, _tempJ))
					count++;
			}
			if (count != 0)
			{
				CCPoint desPoint = cell->getPosition() - ccp(0, count * CELLSIZE);
				cell->moveBy(desPoint,cell->getIndexI() - count,cell->getIndexJ(),count * 0.07f);
			}
		}
	}

}

bool Grid::canTouching(int _indexI, int _indexJ)
{
	if (m_currentTypeCell == kTypeCellZero)
		return true;
	else
	{
		CCObject* it;
		CCARRAY_FOREACH(m_listTouchingCell, it)
		{
			Cell* cell = dynamic_cast<Cell*>(it);

			if (cell != NULL)
			{
				if (cell->canTouching(_indexI, _indexJ))
					return true;
			}
		}
	}
	return false;
}

bool Grid::isListContain(int _indexI, int _indexJ)
{
	CCObject* it;
	CCARRAY_FOREACH(m_listCell, it)
	{
		Cell* cell = dynamic_cast<Cell*>(it);

		if (cell != NULL)
		{
			if(cell->getIndexI() == _indexI && cell->getIndexJ() == _indexJ)
				return true;
		}
	}
	return false;
}