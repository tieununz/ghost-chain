#ifndef __MAINGAME_SCENE_H__
#define __MAINGAME_SCENE_H__
#include "cocos2d.h"
#include "MenuScene.h"
#include "Grid.h"
#include "GameConfig.h"
USING_NS_CC;
class MainGame : public CCLayer
{
	//CC_SYNTHESIZE(Grid*, m_grid, _Grid);
	CC_SYNTHESIZE(CCLayer*, m_mainLayer, MainLayer);
	CC_SYNTHESIZE(CCLayer*, m_pauseLayer, PauseLayer);
	CC_SYNTHESIZE(CCLayer*, m_resultLayer, ResultLayer);

	CCSize visibleSize;
	CCPoint origin;
	
	float m_gameTime;
	CCLabelBMFont *m_gameTimeLabel;
	CCLabelBMFont* scoreLabel;
	CCLabelBMFont* highScoreLabel;

	void update(float dt);
	void soundCallback( CCObject* pSender );
	int m_score;
	int m_highScore;
	void createMainLayer();
	void createPauseLayer();
	CCLabelBMFont *scoreLabelResult;
	CCLabelBMFont *bonusLabelResult;
	CCLabelBMFont *totalLabelResult;
	CCLabelBMFont *highScoreLabelResult;
	void createResultLayer();
	void loginToShare(CCObject* pSender);
	void shareFacebook(CCNode* sender, void *data);
	void shareFacebookCallBack(CCNode* sender, void *data);
	//Handles for level mode
	int m_typeOneRequire;
	int m_typeTwoRequire;
	int m_typeThreeRequire;
	int m_typeFourRequire;
	int m_typeFiveRequire;
	int m_typeSixRequire;
	int m_typeSevenRequire;
	int m_typeEightRequire;
	CCLabelBMFont *m_typeOneRequireLabel;
	CCLabelBMFont *m_typeTwoRequireLabel;
	CCLabelBMFont *m_typeThreeRequireLabel;
	CCLabelBMFont *m_typeFourRequireLabel;
	CCLabelBMFont *m_typeFiveRequireLabel;
	CCLabelBMFont *m_typeSixRequireLabel;
	CCLabelBMFont *m_typeSevenRequireLabel;
	CCLabelBMFont *m_typeEightRequireLabel;
	void setLevel();
	void addLevelHandle(kTagCell _type, int _bonusCount);
	void levelHandle();

	//Button Handle
	void nextLevelHandle(CCObject * pSender);
	void pauseHandle(CCObject * pSender);
	void menuHandle(CCObject * pSender);
	void restartHandle(CCObject * pSender);
	void resumeHandle(CCObject * pSender);

	//Bonus score & time Handle
	void addScoreHandle(int value, CCPoint pos);
	void addTimeHandle(int value);

	void removeFromParrent(CCNode* pSender);
	void setEndGame();
	void completeEndGame();

	CC_SYNTHESIZE(kModeGame, m_modeGame, ModeGame);
	CC_SYNTHESIZE(int, m_level, Level);
public:
	~MainGame(void);
	virtual bool init(kModeGame _modeGame,int _level);
	static CCScene* scene(kModeGame _modeGame,int _level);
	static MainGame * create(kModeGame _modeGame,int _level);
};

#endif // __HELLOWORLD_SCENE_H__