//
//  GameConfig.h
//
//
//  Created by Tieunun on 21/11/2013
//
//

#ifndef __GameConfig__
#define __GameConfig__

#define DevSize CCEGLView::sharedOpenGLView()->getFrameSize()
#define ScaleSulotionY 480*DevSize.height/DevSize.width/800
#define FactorY ((VISIBLESIZE.height - VISIBLESIZE.width * 800 / 480) / 2)
enum {
	kBackground,
	kGridground,
	kCellground,
	kCellgroundEnd,
	kMiddleground,
	kForeground,
	kExplosion,
};

enum kTagCell {
	kTypeCellZero = -1,
	kTypeCellOne = 1,
	kTypeCellTwo = 2,
	kTypeCellThree = 3,
	kTypeCellFour = 4,
	kTypeCellFive = 5,
	kTypeCellSix = 6,
	kTypeCellSeven = 7,
	kTypeCellEight = 8,
};

enum kGameObject {
	kGameObjectCell  = 200,
	kGameObjectGrid  = 201,
	kGameObjectLevelResource  = 202,
};

enum kModeGame {
	kModeGameEasy = 301,
	kModeGameHard = 302,
	kModeGameLevel = 303
};

enum kGameState {
	kGameStateRunning  = 400,
	kGameStatePause  = 401,
	kGameStateEnd  = 402,
};
#define VISIBLESIZE CCDirector::sharedDirector()->getVisibleSize()
#define BONUSSCORE_PERSECOND 100 //LEVEL MODE

#define BONUSTIME_SIX 1
#define BONUSTIME_SEVEN 2
#define BONUSTIME_EIGHT 4

#define SUBTRACTPOINT -100
#define BONUSPOINT_COMMON 100
#define BONUSPOINT_SIX 200
#define BONUSPOINT_SEVEN 300
#define BONUSPOINT_EIGHT 400

#define GAMETIME_EASY 120
#define GAMETIME_NORMAL 120
#define ROW 6
#define COL 8
#define CELLSIZE 78
#define EXTRAPOS ccp((CCEGLView::sharedOpenGLView()->getDesignResolutionSize().width - CELLSIZE * ROW)/2, 25)

#define FactorX (CCEGLView::sharedOpenGLView()->getFrameSize().height / VISIBLESIZE.height)
#define BonusY ((CCEGLView::sharedOpenGLView()->getFrameSize().height - VISIBLESIZE.height) / 2)

#define BONUS_TIME_POS ccp(340, VISIBLESIZE.height - 120 - FactorY)
#define BONUS_TIME_COLOR ccc3(255, 255, 255) 

//Running Game - Main Layer
#define SCORE_POS ccp(170, VISIBLESIZE.height - 40 - FactorY)
#define SCORE_COLOR ccc3(255, 255, 255) 
#define SCORE_FONT "Arial"
#define SCORE_FONTSIZE 35

#define HIGHSCORE_POS ccp(50, VISIBLESIZE.height - 88 - FactorY)
#define HIGHSCORE_COLOR ccc3(255, 255, 255) 
#define HIGHSCORE_FONT "Arial"
#define HIGHSCORE_FONTSIZE 27

#define GAMETIME_POS ccp(418, VISIBLESIZE.height - 34 - FactorY)
#define GAMETIME_COLOR ccc3(255, 255, 255) 
#define GAMETIME_FONT "Arial"
#define GAMETIME_FONTSIZE 40

#define LEVEL_POS ccp(418, VISIBLESIZE.height - 34 - FactorY)
#define LEVEL_COLOR ccc3(255, 255, 255) 
#define LEVEL_FONT "Arial"
#define LEVEL_FONTSIZE 40

#define PAUSE_BUTTON_POS ccp(450, VISIBLESIZE.height - 34 - FactorY)

//Pause Game - Pause Layer
#define MENU_BUTTON_POS ccp(187, VISIBLESIZE.height - 478 - FactorY)
#define RESUME_BUTTON_POS ccp(197, VISIBLESIZE.height - 297 - FactorY)
#define RESTART_BUTTON_POS ccp(199, VISIBLESIZE.height - 392 - FactorY)
#define MUSIC_BUTTON_POS ccp(390, VISIBLESIZE.height - 250 - FactorY)

//Result Game - Result Layer
#define MENUEND_LEVEL_BUTTON_POS ccp(111, VISIBLESIZE.height - 732 - FactorY)
#define MENUEND_CLASSIC_BUTTON_POS ccp(237, VISIBLESIZE.height - 731 - FactorY)
#define NEXTLEVEL_BUTTON_POS ccp(239, VISIBLESIZE.height - 622 - FactorY)
#define RETRY_BUTTON_POS ccp(366, VISIBLESIZE.height - 733 - FactorY)
#define PLAYAGAIN_BUTTON_POS ccp(239, VISIBLESIZE.height - 622 - FactorY)

#define LEVEL_RESULT_POS ccp(220, VISIBLESIZE.height - 170 - FactorY)
#define LEVEL_RESULT_COLOR ccc3(255, 255, 255) 
#define LEVEL_RESULT_FONT "Arial"
#define LEVEL_RESULT_FONTSIZE 34

#define SCORE_RESULT_POS ccp(144 , VISIBLESIZE.height - 266 - FactorY)
#define SCORE_RESULT_COLOR ccc3(255, 255, 255) 
#define SCORE_RESULT_FONT "Arial"
#define SCORE_RESULT_FONTSIZE 34

#define BONUS_RESULT_POS ccp(150, VISIBLESIZE.height - 354 - FactorY)
#define BONUS_RESULT_COLOR ccc3(255, 255, 255) 
#define BONUS_RESULT_FONT "Arial"
#define BONUS_RESULT_FONTSIZE 34

#define TOTAL_RESULT_POS ccp(144, VISIBLESIZE.height - 440 - FactorY)
#define TOTAL_RESULT_COLOR ccc3(255, 255, 255) 
#define TOTAL_RESULT_FONT "Arial"
#define TOTAL_RESULT_FONTSIZE 34

#define CLASSIC_RESULT_POS ccp(220, VISIBLESIZE.height - 170 - FactorY)
#define CLASSIC_RESULT_COLOR ccc3(255, 255, 255) 
#define CLASSIC_RESULT_FONT "Arial"
#define CLASSIC_RESULT_FONTSIZE 34

#define HIGHSCORE_RESULT_POS ccp(212, VISIBLESIZE.height - 354 - FactorY)
#define HIGHSCORE_RESULT_COLOR ccc3(255, 255, 255) 
#define HIGHSCORE_RESULT_FONT "Arial"
#define HIGHSCORE_RESULT_FONTSIZE 34

//Set Level - Level Handle

#define TYPE_REQUIRE_LABEL_COLOR ccc3(255, 255, 255) 
#define TYPE_REQUIRE_LABEL_FONT "Arial"
#define TYPE_REQUIRE_LABEL_FONTSIZE 25

#define TYPE_REQUIRE_LABEL_POS1 ccp(82, VISIBLESIZE.height - 89 - FactorY)
#define TYPE_REQUIRE_LABEL_POS2 (TYPE_REQUIRE_LABEL_POS1 + ccp(70,0))
#define TYPE_REQUIRE_LABEL_POS3 (TYPE_REQUIRE_LABEL_POS2 + ccp(70,0))

#define TYPE_REQUIRE_SPRITE_POS1 ccp(66, VISIBLESIZE.height - 89 - FactorY)
#define TYPE_REQUIRE_SPRITE_POS2 (TYPE_REQUIRE_SPRITE_POS1 + ccp(70,0))
#define TYPE_REQUIRE_SPRITE_POS3 (TYPE_REQUIRE_SPRITE_POS2 + ccp(70,0))

//Select Level Scene
#define LEVEL01 ccp(120, VISIBLESIZE.height - 220 - FactorY)
#define LEVEL02 ccp(240, VISIBLESIZE.height - 220 - FactorY)
#define LEVEL03 ccp(360, VISIBLESIZE.height - 220 - FactorY)
#define LEVEL04 ccp(120, VISIBLESIZE.height - 336 - FactorY)
#define LEVEL05 ccp(240, VISIBLESIZE.height - 336 - FactorY)
#define LEVEL06 ccp(360, VISIBLESIZE.height - 336 - FactorY)
#define LEVEL07 ccp(120, VISIBLESIZE.height - 452 - FactorY)
#define LEVEL08 ccp(240, VISIBLESIZE.height - 452 - FactorY)
#define LEVEL09 ccp(360, VISIBLESIZE.height - 452 - FactorY)
#define LEVEL10 ccp(120, VISIBLESIZE.height - 568 - FactorY)
#define LEVEL11 ccp(240, VISIBLESIZE.height - 568 - FactorY)
#define LEVEL12 ccp(360, VISIBLESIZE.height - 568 - FactorY)
#define LEVEL13 ccp(600, VISIBLESIZE.height - 220 - FactorY)
#define LEVEL14 ccp(720, VISIBLESIZE.height - 220 - FactorY)
#define LEVEL15 ccp(840, VISIBLESIZE.height - 220 - FactorY)
#define LEVEL16 ccp(600, VISIBLESIZE.height - 336 - FactorY)
#define LEVEL17 ccp(720, VISIBLESIZE.height - 336 - FactorY)
#define LEVEL18 ccp(840, VISIBLESIZE.height - 336 - FactorY)
#define LEVEL19 ccp(600, VISIBLESIZE.height - 452 - FactorY)
#define LEVEL20 ccp(720, VISIBLESIZE.height - 452 - FactorY)
#define LEVEL21 ccp(840, VISIBLESIZE.height - 452 - FactorY)
#define LEVEL22 ccp(600, VISIBLESIZE.height - 568 - FactorY)
#define LEVEL23 ccp(720, VISIBLESIZE.height - 568 - FactorY)
#define LEVEL24 ccp(840, VISIBLESIZE.height - 568 - FactorY)

//---

#endif