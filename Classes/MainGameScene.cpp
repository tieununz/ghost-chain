#include "MainGameScene.h"
#include "GameConfig.h"
#include "SimpleAudioEngine.h"
#include "SoundManager.h"
#include "NDKHelper/NDKHelper.h"
using namespace CocosDenshion;

CCScene* MainGame::scene(kModeGame _modeGame,int _level)
{
	CCScene *scene = CCScene::create();
	MainGame *layer = MainGame::create(_modeGame,_level);
	scene->addChild(layer);
	return scene;
}

MainGame * MainGame::create(kModeGame _modeGame,int _level)
{
	MainGame *mainGame = new MainGame();
	if(mainGame)
	{
		mainGame->init(_modeGame,_level);
		mainGame->autorelease();
		return mainGame;
	}
	CC_SAFE_DELETE(mainGame);
	return NULL;
}

MainGame::~MainGame(void)
{
	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	NDKHelper::RemoveSelectorsInGroup("HelloWorldSelectors");
#endif
}

// on "init" you need to initialize your instance
bool MainGame::init(kModeGame _modeGame,int _level)
{
	//////////////////////////////
	// 1. super init first
	if ( !CCLayer::init() )
	{
		return false;
	}
	//this->setTouchEnabled(true);

	visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	origin = CCDirector::sharedDirector()->getVisibleOrigin();
	float a = ScaleSulotionY;
	m_modeGame = _modeGame;
	m_level = _level;
	
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("Explosion.plist");

	createMainLayer();
	createPauseLayer();
	createResultLayer();

	setLevel();

	scheduleUpdate();
	return true;
}

void MainGame::update(float dt)
{
	if(m_modeGame != kModeGameLevel && m_gameTime != 0)
	{
		Grid *m_grid = (Grid*)m_mainLayer->getChildByTag(kGameObjectGrid);
		if(m_grid->getStateGame() == kGameStateRunning)
			m_gameTime -= dt;
		if(m_gameTime <= 0)
			m_gameTime = 0;
		char szName[20] = {0};
		int min = (int)m_gameTime / 60;
		int sec = (int)m_gameTime % 60;
		if(sec < 10)
		{
			sprintf(szName, "%d : 0%d", min, sec);

		}
		else
		{
			sprintf(szName, "%d : %d", min, sec);
		}		
		m_gameTimeLabel->setString(szName);
		if(m_gameTime <= 0)
		{
			setEndGame();
		}
	}
	else
	{
		m_gameTime -= dt;
		if(m_gameTime <= 0)
			m_gameTime = 0;
	}
}

void MainGame::createMainLayer()
{
	m_mainLayer = CCLayer::create();
	this->addChild(m_mainLayer);
	m_mainLayer->setPosition(0, 0);

	CCSprite* pBG = CCSprite::create("BG_Game.png");
	pBG->setPosition(ccp(visibleSize.width/2 + origin.x, (visibleSize.height/2 + origin.y)));
	m_mainLayer->addChild(pBG, kMiddleground);
	
	CCSprite* pBank = CCSprite::create("BG_Blank.png");
	pBank->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	m_mainLayer->addChild(pBank, kBackground);

	Grid *m_grid = Grid::create(m_modeGame);
	m_grid->setPosition(0, (visibleSize.height - visibleSize.width * 800 / 480) / 2);
	m_mainLayer->addChild(m_grid,kGridground,kGameObjectGrid);

	CCMenuItem *pauseButton = CCMenuItemImage::create(
		"Pause_Button.png",
		"Pause_Button.png",
		this,
		menu_selector(MainGame::pauseHandle) );
	pauseButton->setPosition(PAUSE_BUTTON_POS - ccp(0,pauseButton->getContentSize().height));
	CCMenu * pMenu = CCMenu::create(pauseButton, NULL);
	pMenu->setPosition(ccp(0,pauseButton->getContentSize().height));
	m_mainLayer->addChild(pMenu, kBackground);
	
	m_score = 0;
	char szName[20] = {0};
	sprintf(szName, "%i", m_score);

	scoreLabel = CCLabelBMFont::create("12345", "Mia_64.fnt");
	scoreLabel->setCString(szName);
	scoreLabel->setScale(0.5f);
	scoreLabel->setColor(SCORE_COLOR);
	scoreLabel->setAnchorPoint(ccp(0.0f,0.5f));
	scoreLabel->setPosition(SCORE_POS);
	m_mainLayer->addChild(scoreLabel,kForeground);

	if(m_modeGame != kModeGameLevel)
	{
		if(m_modeGame == kModeGameEasy)
			m_highScore = CCUserDefault::sharedUserDefault()->getIntegerForKey("HighScoreEasy");
		if(m_modeGame == kModeGameHard)
			m_highScore = CCUserDefault::sharedUserDefault()->getIntegerForKey("HighScoreHard");
		sprintf(szName, "HighScore:%i", m_highScore);
		
		highScoreLabel = CCLabelBMFont::create("12345", "Mia_64.fnt");
		highScoreLabel->setCString(szName);
		highScoreLabel->setScale(0.5f);
		highScoreLabel->setColor(HIGHSCORE_COLOR);
		highScoreLabel->setAnchorPoint(ccp(0.0f,0.5f));
		highScoreLabel->setPosition(HIGHSCORE_POS);
		m_mainLayer->addChild(highScoreLabel,kForeground);

		if(m_modeGame == kModeGameEasy)
			m_gameTime = GAMETIME_EASY;
		else m_gameTime = GAMETIME_NORMAL;

		m_gameTimeLabel = CCLabelBMFont::create("12345", "Mia_64.fnt");
		m_gameTimeLabel->setScale(0.5f);
		m_gameTimeLabel->setColor(GAMETIME_COLOR);
		m_gameTimeLabel->setAnchorPoint(ccp(1.0f,0.5f));
		m_gameTimeLabel->setPosition(GAMETIME_POS);
		m_mainLayer->addChild(m_gameTimeLabel,kForeground);
	}
	else
	{
		sprintf(szName, "Lv %i", m_level);

		CCLabelBMFont *levelLabel = CCLabelBMFont::create("12345", "Mia_64.fnt");
		levelLabel->setCString(szName);
		levelLabel->setScale(0.5f);
		levelLabel->setColor(LEVEL_COLOR);
		levelLabel->setAnchorPoint(ccp(1.0f,0.5f));
		levelLabel->setPosition(LEVEL_POS);
		m_mainLayer->addChild(levelLabel,kForeground);
	}
}
void MainGame::createPauseLayer()
{
	m_pauseLayer = CCLayer::create();
	this->addChild(m_pauseLayer);
	m_pauseLayer->setVisible(false);
	CCSprite* pPauseBG = CCSprite::create("BG_Pause.png");
	pPauseBG->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	m_pauseLayer->addChild(pPauseBG, kBackground);

	CCMenuItem *menuButton = CCMenuItemImage::create(
		"Menu_Button.png",
		"Menu_Button.png",
		this,
		menu_selector(MainGame::menuHandle) );

	CCMenuItem *resumeButton = CCMenuItemImage::create(
		"Resume_Button.png",
		"Resume_Button.png",
		this,
		menu_selector(MainGame::resumeHandle) );

	CCMenuItem *restartButton = CCMenuItemImage::create(
		"Restart_Button.png",
		"Restart_Button.png",
		this,
		menu_selector(MainGame::restartHandle) );

	CCMenuItem* soundOn = CCMenuItemImage::create("MusicOn_Button.png.png", NULL, NULL);
	CCMenuItem* soundOff = CCMenuItemImage::create("MusicOff_Button.png", NULL, NULL);
	CCMenuItemToggle* soundToggle = CCMenuItemToggle::createWithTarget(this,  menu_selector(MainGame::soundCallback), soundOn, soundOff, NULL);

	if(SoundManager::getInstance()->isEnableBackground())
	{
		soundToggle->setSelectedIndex(0);
	}
	else
	{
		soundToggle->setSelectedIndex(1);
	}

	menuButton->setPosition(MENU_BUTTON_POS);
	resumeButton->setPosition(RESUME_BUTTON_POS);
	restartButton->setPosition(RESTART_BUTTON_POS);
	soundToggle->setPosition(MUSIC_BUTTON_POS);
	CCMenu * pMenu = CCMenu::create(menuButton,resumeButton,restartButton,soundToggle, NULL);
	pMenu->setPosition(ccp(0, 0));
	m_pauseLayer->addChild(pMenu, kBackground);
}

void MainGame::soundCallback( CCObject* pSender )
{
	if(SoundManager::getInstance()->isEnableBackground())
	{
		SoundManager::getInstance()->setEnableBackground(false);
		SoundManager::getInstance()->setEnableEffect(false);

		//
		SoundManager::getInstance()->stopBackground();
	}
	else
	{
		SoundManager::getInstance()->setEnableBackground(true);
		SoundManager::getInstance()->setEnableEffect(true);

		//
		SoundManager::getInstance()->playBackground("Sounds/bgSong.mp3");
	}
}

void MainGame::createResultLayer()
{
	if(m_modeGame == kModeGameLevel)
	{
		m_resultLayer = CCLayer::create();
		this->addChild(m_resultLayer);
		m_resultLayer->setVisible(false);
		CCSprite* pResultBG = CCSprite::create("BG_Level_Mode_Done.png");
		pResultBG->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
		m_resultLayer->addChild(pResultBG, kBackground);

		CCMenuItem *menuButton = CCMenuItemImage::create(
			"Menu_Button_End.png",
			"Menu_Button_End.png",
			this,
			menu_selector(MainGame::menuHandle) );

		CCMenuItem *nextLevelButton = CCMenuItemImage::create(
			"NextLevel_Button.png",
			"NextLevel_Button.png",
			this,
			menu_selector(MainGame::nextLevelHandle) );

		CCMenuItem *retryButton = CCMenuItemImage::create(
			"Retry_Button.png",
			"Retry_Button.png",
			this,
			menu_selector(MainGame::restartHandle) );

		CCMenuItem *shareButton = CCMenuItemImage::create(
			"Share_Button.png",
			"Share_Button.png",
			this,
			menu_selector(MainGame::loginToShare) );

		shareButton->setPosition(MENUEND_LEVEL_BUTTON_POS);
		menuButton->setPosition(MENUEND_LEVEL_BUTTON_POS);
		nextLevelButton->setPosition(NEXTLEVEL_BUTTON_POS);
		retryButton->setPosition(RETRY_BUTTON_POS);

		CCMenu * pMenu = CCMenu::create(menuButton,nextLevelButton,retryButton,shareButton, NULL);
		pMenu->setPosition(ccp(0, 0));
		m_resultLayer->addChild(pMenu, kMiddleground);

		char szName[20] = {0};
		sprintf(szName, "Level %i Complete", m_level);

		CCLabelBMFont* levelLabelResult = CCLabelBMFont::create("12345", "Mia_64.fnt");
		levelLabelResult->setScale(0.5f);
		levelLabelResult->setColor(LEVEL_RESULT_COLOR);
		levelLabelResult->setAnchorPoint(ccp(0.5f,0.5f));
		levelLabelResult->setString(szName);
		levelLabelResult->setPosition(LEVEL_RESULT_POS);
		m_resultLayer->addChild(levelLabelResult,kMiddleground);


		scoreLabelResult = CCLabelBMFont::create("12345", "Mia_64.fnt");
		scoreLabelResult->setScale(0.5f);
		scoreLabelResult->setColor(SCORE_RESULT_COLOR);
		scoreLabelResult->setAnchorPoint(ccp(0.0f,0.5f));
		scoreLabelResult->setPosition(SCORE_RESULT_POS);
		m_resultLayer->addChild(scoreLabelResult,kMiddleground);

		bonusLabelResult = CCLabelBMFont::create("12345", "Mia_64.fnt");
		bonusLabelResult->setScale(0.5f);
		bonusLabelResult->setColor(BONUS_RESULT_COLOR);
		bonusLabelResult->setAnchorPoint(ccp(0.0f,0.5f));
		bonusLabelResult->setPosition(BONUS_RESULT_POS);
		m_resultLayer->addChild(bonusLabelResult,kMiddleground);

		totalLabelResult = CCLabelBMFont::create("12345", "Mia_64.fnt");
		totalLabelResult->setScale(0.5f);
		totalLabelResult->setColor(TOTAL_RESULT_COLOR);
		totalLabelResult->setAnchorPoint(ccp(0.0f,0.5f));
		totalLabelResult->setPosition(TOTAL_RESULT_POS);
		m_resultLayer->addChild(totalLabelResult,kMiddleground);
	}
	else
	{
		m_resultLayer = CCLayer::create();
		this->addChild(m_resultLayer);
		m_resultLayer->setVisible(false);
		CCSprite* pResultBG = CCSprite::create("BG_Classic_Mode_Done.png");
		pResultBG->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
		m_resultLayer->addChild(pResultBG, kBackground);

		CCMenuItem *menuButton = CCMenuItemImage::create(
			"Menu_Button_End.png",
			"Menu_Button_End.png",
			this,
			menu_selector(MainGame::menuHandle) );

		CCMenuItem *playAgainButton = CCMenuItemImage::create(
			"PlayAgain_Button.png",
			"PlayAgain_Button.png",
			this,
			menu_selector(MainGame::restartHandle) );
		CCMenuItem *shareButton = CCMenuItemImage::create(
			"Share_Button.png",
			"Share_Button.png",
			this,
			menu_selector(MainGame::loginToShare) );

		shareButton->setPosition(MENUEND_LEVEL_BUTTON_POS);
		menuButton->setPosition(MENUEND_CLASSIC_BUTTON_POS);
		playAgainButton->setPosition(PLAYAGAIN_BUTTON_POS);

		CCMenu * pMenu = CCMenu::create(menuButton,playAgainButton,shareButton, NULL);
		pMenu->setPosition(ccp(0, 0));
		m_resultLayer->addChild(pMenu, kMiddleground);

		char szName[20] = {0};

		if(m_modeGame == kModeGameEasy)
		{
			CCLabelBMFont * classicLabelResult = CCLabelBMFont::create("Complete Easy Mode", "Mia_64.fnt");
			classicLabelResult->setScale(0.5f);
			classicLabelResult->setColor(CLASSIC_RESULT_COLOR);
			classicLabelResult->setAnchorPoint(ccp(0.5f,0.5f));
			classicLabelResult->setPosition(CLASSIC_RESULT_POS);
			m_resultLayer->addChild(classicLabelResult,kMiddleground);
		}
		else
		{
			CCLabelBMFont * classicLabelResult = CCLabelBMFont::create("Complete Normal Mode", "Mia_64.fnt");
			classicLabelResult->setScale(0.5f);
			classicLabelResult->setColor(CLASSIC_RESULT_COLOR);
			classicLabelResult->setAnchorPoint(ccp(0.5f,0.5f));
			classicLabelResult->setPosition(CLASSIC_RESULT_POS);
			m_resultLayer->addChild(classicLabelResult,kMiddleground);
		}

		scoreLabelResult = CCLabelBMFont::create("12345", "Mia_64.fnt");
		scoreLabelResult->setScale(0.5f);
		scoreLabelResult->setColor(SCORE_RESULT_COLOR);
		scoreLabelResult->setAnchorPoint(ccp(0.0f,0.5f));
		scoreLabelResult->setPosition(SCORE_RESULT_POS);
		m_resultLayer->addChild(scoreLabelResult,kMiddleground);

		highScoreLabelResult = CCLabelBMFont::create("12345", "Mia_64.fnt");
		highScoreLabelResult->setScale(0.5f);
		highScoreLabelResult->setColor(HIGHSCORE_RESULT_COLOR);
		highScoreLabelResult->setAnchorPoint(ccp(0.0f,0.5f));
		highScoreLabelResult->setPosition(HIGHSCORE_RESULT_POS);
		m_resultLayer->addChild(highScoreLabelResult,kMiddleground);
	}
}

void MainGame::loginToShare(CCObject* pSender)
{
	CCLog("LoginFacebook");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	NDKHelper::AddSelector("HelloWorldSelectors",
		"shareFacebook",
		callfuncND_selector(MainGame::shareFacebook),
		this);
	
	CCDictionary* prms = CCDictionary::create();
	prms->setObject(CCString::create("shareFacebook"), "to_be_called");

	SendMessageWithParams(string("LoginFacebook"), prms);
#endif
}
void MainGame::shareFacebook(CCNode* sender, void *data)
{
	CCLog("shAREFACEBOK");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	NDKHelper::AddSelector("HelloWorldSelectors",
		"shareFacebookCallBack",
		callfuncND_selector(MainGame::shareFacebookCallBack),
		this);

	CCDictionary* prms = CCDictionary::create();
	prms->setObject(CCString::create("shareFacebookCallBack"), "to_be_called");

	char _buffer[100];
	if(m_modeGame == kModeGameEasy)
	{
		int a = 10;
		sprintf(_buffer,"I gained %s scores in Easy Mode of Ghost Chain", scoreLabelResult->getString());
	}
	if(m_modeGame == kModeGameHard)
	{
		int a = 10;
		sprintf(_buffer,"I gained %s scores in Easy Mode of Ghost Chain", scoreLabelResult->getString());
	}
	if(m_modeGame == kModeGameLevel)
	{
		int a = 10;
		sprintf(_buffer,"I have completed level %i with %s scores in Level Mode of Ghost Chain game.",m_level,totalLabelResult->getString());
	}
	
	prms->setObject(CCString::create(_buffer), "description");

	SendMessageWithParams(string("ShareOnwall"), prms);
	CCLog(CCString::create(_buffer)->getCString());
#endif
}

void MainGame::shareFacebookCallBack(CCNode* sender, void *data)
{
	CCLog("shareFacebookCallBack");
}
void MainGame::setLevel()
{
	m_typeOneRequire = 0;
	m_typeTwoRequire = 0;
	m_typeThreeRequire = 0;
	m_typeFourRequire = 0;
	m_typeFiveRequire = 0;
	m_typeSixRequire = 0;
	m_typeSevenRequire = 0;
	m_typeEightRequire = 0;

	CCSprite* m_pTypeOne = CCSprite::create("Type01_NoTouch_1.png");
	m_pTypeOne->setScale(0.3f);
	CCSprite* m_pTypeTwo = CCSprite::create("Type02_NoTouch_1.png");
	m_pTypeTwo->setScale(0.3f);
	CCSprite* m_pTypeThree = CCSprite::create("Type03_NoTouch_1.png");
	m_pTypeThree->setScale(0.3f);
	CCSprite* m_pTypeFour = CCSprite::create("Type04_NoTouch_1.png");
	m_pTypeFour->setScale(0.3f);
	CCSprite* m_pTypeFive = CCSprite::create("Type05_NoTouch_1.png");
	m_pTypeFive->setScale(0.3f);
	CCSprite* m_pTypeSix = CCSprite::create("Type06_NoTouch_1.png");
	m_pTypeSix->setScale(0.3f);
	CCSprite* m_pTypeSeven = CCSprite::create("Type07_NoTouch_1.png");
	m_pTypeSeven->setScale(0.3f);
	CCSprite* m_pTypeEight = CCSprite::create("Type08_NoTouch_1.png");
	m_pTypeEight->setScale(0.3f);


	char szName[20] = {0};

	m_typeOneRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeOneRequireLabel->setScale(0.5f);
	m_typeOneRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeOneRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeOneRequireLabel,kForeground);

	m_typeTwoRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeTwoRequireLabel->setScale(0.5f);
	m_typeTwoRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeTwoRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeTwoRequireLabel,kForeground);

	m_typeThreeRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeThreeRequireLabel->setScale(0.5f);
	m_typeThreeRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeThreeRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeThreeRequireLabel,kForeground);

	m_typeFourRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeFourRequireLabel->setScale(0.5f);
	m_typeFourRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeFourRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeFourRequireLabel,kForeground);

	m_typeFiveRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeFiveRequireLabel->setScale(0.5f);
	m_typeFiveRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeFiveRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeFiveRequireLabel,kForeground);

	m_typeSixRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeSixRequireLabel->setScale(0.5f);
	m_typeSixRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeSixRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeSixRequireLabel,kForeground);

	m_typeSevenRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeSevenRequireLabel->setScale(0.5f);
	m_typeSevenRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeSevenRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeSevenRequireLabel,kForeground);

	m_typeEightRequireLabel = CCLabelBMFont::create("123", "Mia_64.fnt");
	m_typeEightRequireLabel->setScale(0.5f);
	m_typeEightRequireLabel->setColor(TYPE_REQUIRE_LABEL_COLOR);
	m_typeEightRequireLabel->setAnchorPoint(ccp(0.5f,0.5f));
	m_mainLayer->addChild(m_typeEightRequireLabel,kForeground);

	m_typeOneRequireLabel->setVisible(false);
	m_typeTwoRequireLabel->setVisible(false);
	m_typeThreeRequireLabel->setVisible(false);
	m_typeFourRequireLabel->setVisible(false);
	m_typeFiveRequireLabel->setVisible(false);
	m_typeSixRequireLabel->setVisible(false);
	m_typeSevenRequireLabel->setVisible(false);
	m_typeEightRequireLabel->setVisible(false);

	CCPoint pos1 = TYPE_REQUIRE_SPRITE_POS1;
	CCPoint pos2 = TYPE_REQUIRE_SPRITE_POS2;
	CCPoint pos3 = TYPE_REQUIRE_SPRITE_POS3;
	CCPoint posLabel1 = TYPE_REQUIRE_LABEL_POS1;
	CCPoint posLabel2 = TYPE_REQUIRE_LABEL_POS2;
	CCPoint posLabel3 = TYPE_REQUIRE_LABEL_POS3;

	if(m_level > 24)
		m_level = 24;
	switch(m_level)
	{
	case 1: //8 type 01, 8 type 02
		// type 1
		m_typeOneRequire = 8;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		m_typeOneRequireLabel->setPosition(posLabel1);
		m_typeOneRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeOneRequireLabel->setVisible(true);
		m_pTypeOne->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeOne,kForeground);

		// type 2
		m_typeTwoRequire = 8;
		sprintf(szName, "x%i", m_typeTwoRequire);
		m_typeTwoRequireLabel->setString(szName);
		m_typeTwoRequireLabel->setPosition(posLabel2);
		m_typeTwoRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeTwoRequireLabel->setVisible(true);
		m_pTypeTwo->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeTwo,kForeground);
		m_gameTime = 60.0f;
		break;
	case 2://8 type 02, 8 type 04, 8 type 05
		//type 2
		m_typeTwoRequire = 8;
		sprintf(szName, "x%i", m_typeTwoRequire);
		m_typeTwoRequireLabel->setString(szName);
		m_typeTwoRequireLabel->setPosition(posLabel1);
		m_typeTwoRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeTwoRequireLabel->setVisible(true);
		m_pTypeTwo->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeTwo,kForeground);

		//type 4
		m_typeFourRequire = 8;
		sprintf(szName, "x%i", m_typeFourRequire);
		m_typeFourRequireLabel->setString(szName);
		m_typeFourRequireLabel->setPosition(posLabel2);
		m_typeFourRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFourRequireLabel->setVisible(true);
		m_pTypeFour->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeFour,kForeground);

		//type 5
		m_typeFiveRequire = 8;
		sprintf(szName, "x%i", m_typeFiveRequire);
		m_typeFiveRequireLabel->setString(szName);
		m_typeFiveRequireLabel->setPosition(posLabel3);
		m_typeFiveRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFiveRequireLabel->setVisible(true);
		m_pTypeFive->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeFive,kForeground);

		m_gameTime = 80.0f;
		break;
	case 3://15 type 03, 15 type 04
		//type 3
		m_typeThreeRequire = 15;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		m_typeThreeRequireLabel->setPosition(posLabel1);
		m_typeThreeRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeThreeRequireLabel->setVisible(true);
		m_pTypeThree->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeThree,kForeground);

		//type 4
		m_typeFourRequire = 15;
		sprintf(szName, "x%i", m_typeFourRequire);
		m_typeFourRequireLabel->setString(szName);
		m_typeFourRequireLabel->setPosition(posLabel2);
		m_typeFourRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFourRequireLabel->setVisible(true);
		m_pTypeFour->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeFour,kForeground);

		m_gameTime = 90.0f;
		break;
	case 4://20 type 01, 20 type 05
		// type 1
		m_typeOneRequire = 20;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		m_typeOneRequireLabel->setPosition(posLabel1);
		m_typeOneRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeOneRequireLabel->setVisible(true);
		m_pTypeOne->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeOne,kForeground);
		//type 5
		m_typeFiveRequire = 20;
		sprintf(szName, "x%i", m_typeFiveRequire);
		m_typeFiveRequireLabel->setString(szName);
		m_typeFiveRequireLabel->setPosition(posLabel2);
		m_typeFiveRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFiveRequireLabel->setVisible(true);
		m_pTypeFive->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeFive,kForeground);

		m_gameTime = 120.0f;
		break;
	case 5://30 type 03, 30 type 04
		//type 3
		m_typeThreeRequire = 30;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		m_typeThreeRequireLabel->setPosition(posLabel1);
		m_typeThreeRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeThreeRequireLabel->setVisible(true);
		m_pTypeThree->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeThree,kForeground);

		//type 4
		m_typeFourRequire = 30;
		sprintf(szName, "x%i", m_typeFourRequire);
		m_typeFourRequireLabel->setString(szName);
		m_typeFourRequireLabel->setPosition(posLabel2);
		m_typeFourRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFourRequireLabel->setVisible(true);
		m_pTypeFour->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeFour,kForeground);

		m_gameTime = 150.0f;
		break;
	case 6://20 type 03, 6 type 06
		//type 3
		m_typeThreeRequire = 20;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		m_typeThreeRequireLabel->setPosition(posLabel1);
		m_typeThreeRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeThreeRequireLabel->setVisible(true);
		m_pTypeThree->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeThree,kForeground);
		
		//type 6
		m_typeSixRequire = 6;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		m_typeSixRequireLabel->setPosition(posLabel2);
		m_typeSixRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSixRequireLabel->setVisible(true);
		m_pTypeSix->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSix,kForeground);

		m_gameTime = 180.0f;
		break;
	case 7://20 type 01, 10 type 06, 5 type 07
		// type 1
		m_typeOneRequire = 20;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		m_typeOneRequireLabel->setPosition(posLabel1);
		m_typeOneRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeOneRequireLabel->setVisible(true);
		m_pTypeOne->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeOne,kForeground);

		//type 6
		m_typeSixRequire = 10;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		m_typeSixRequireLabel->setPosition(posLabel2);
		m_typeSixRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSixRequireLabel->setVisible(true);
		m_pTypeSix->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSix,kForeground);

		//type 7
		m_typeSevenRequire = 5;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel3);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		m_gameTime = 210.0f;
		break;
	case 8://25 type 03, 5 type 08
		//type 3
		m_typeThreeRequire = 25;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		m_typeThreeRequireLabel->setPosition(posLabel1);
		m_typeThreeRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeThreeRequireLabel->setVisible(true);
		m_pTypeThree->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeThree,kForeground);

		//type 8
		m_typeEightRequire = 5;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel2);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 240.0f;
		break;
	case 9://25 type 04, 10 type 07, 5 type 08
		//type 4
		m_typeFourRequire = 25;
		sprintf(szName, "x%i", m_typeFourRequire);
		m_typeFourRequireLabel->setString(szName);
		m_typeFourRequireLabel->setPosition(posLabel1);
		m_typeFourRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFourRequireLabel->setVisible(true);
		m_pTypeFour->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeFour,kForeground);

		//type 7
		m_typeSevenRequire = 10;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel2);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		//type 8
		m_typeEightRequire = 5;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 270.0f;
		break;
	case 10://30 type 01, 15 type 06, 10 type 08
		// type 1
		m_typeOneRequire = 30;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		m_typeOneRequireLabel->setPosition(posLabel1);
		m_typeOneRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeOneRequireLabel->setVisible(true);
		m_pTypeOne->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeOne,kForeground);

		//type 6
		m_typeSixRequire = 15;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		m_typeSixRequireLabel->setPosition(posLabel2);
		m_typeSixRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSixRequireLabel->setVisible(true);
		m_pTypeSix->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSix,kForeground);

		//type 8
		m_typeEightRequire = 10;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 300.0f;
		break;
	case 11://15 type 06, 15 type 07, 10 type 08
		//type 6
		m_typeSixRequire = 15;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		m_typeSixRequireLabel->setPosition(posLabel1);
		m_typeSixRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSixRequireLabel->setVisible(true);
		m_pTypeSix->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeSix,kForeground);

		//type 7
		m_typeSevenRequire = 15;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel2);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		//type 8
		m_typeEightRequire = 10;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 360.0f;
		break;
	case 12://20 type 08
		//type 8
		m_typeEightRequire = 20;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel1);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 390.0f;
		break;
	case 13://30 type 01, 20 type 07
		// type 1
		m_typeOneRequire = 30;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		m_typeOneRequireLabel->setPosition(posLabel1);
		m_typeOneRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeOneRequireLabel->setVisible(true);
		m_pTypeOne->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeOne,kForeground);

		//type 7
		m_typeSevenRequire = 20;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel2);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		m_gameTime = 420.0f;
		break;
	case 14://40 type 02, 10 type 06, 10 type 08
		//type 2
		m_typeTwoRequire = 40;
		sprintf(szName, "x%i", m_typeTwoRequire);
		m_typeTwoRequireLabel->setString(szName);
		m_typeTwoRequireLabel->setPosition(posLabel1);
		m_typeTwoRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeTwoRequireLabel->setVisible(true);
		m_pTypeTwo->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeTwo,kForeground);

		//type 6
		m_typeSixRequire = 10;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		m_typeSixRequireLabel->setPosition(posLabel2);
		m_typeSixRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSixRequireLabel->setVisible(true);
		m_pTypeSix->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSix,kForeground);

		//type 8
		m_typeEightRequire = 10;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 450.0f;
		break;
	case 15://60 type 03, 15 type 08
		//type 3
		m_typeThreeRequire =60 ;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		m_typeThreeRequireLabel->setPosition(posLabel1);
		m_typeThreeRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeThreeRequireLabel->setVisible(true);
		m_pTypeThree->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeThree,kForeground);

		//type 8
		m_typeEightRequire = 15;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel2);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 480.0f;
		break;
	case 16://60 type 04, 60 type 05
		//type 4
		m_typeFourRequire = 60;
		sprintf(szName, "x%i", m_typeFourRequire);
		m_typeFourRequireLabel->setString(szName);
		m_typeFourRequireLabel->setPosition(posLabel1);
		m_typeFourRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFourRequireLabel->setVisible(true);
		m_pTypeFour->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeFour,kForeground);

		//type 5
		m_typeFiveRequire = 60;
		sprintf(szName, "x%i", m_typeFiveRequire);
		m_typeFiveRequireLabel->setString(szName);
		m_typeFiveRequireLabel->setPosition(posLabel2);
		m_typeFiveRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeFiveRequireLabel->setVisible(true);
		m_pTypeFive->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeFive,kForeground);

		m_gameTime = 510.0f;
		break;
	case 17://60 type 03, 15 type 07, 20 type 08
		//type 3
		m_typeThreeRequire = 60;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		m_typeThreeRequireLabel->setPosition(posLabel1);
		m_typeThreeRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeThreeRequireLabel->setVisible(true);
		m_pTypeThree->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeThree,kForeground);

		//type 7
		m_typeSevenRequire = 15;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel2);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		//type 8
		m_typeEightRequire = 20;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 540.0f;
		break;
	case 18://60 type 01, 60 type 02, 20 type 07
		// type 1
		m_typeOneRequire = 60;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		m_typeOneRequireLabel->setPosition(posLabel1);
		m_typeOneRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeOneRequireLabel->setVisible(true);
		m_pTypeOne->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeOne,kForeground);

		//type 2
		m_typeTwoRequire = 60;
		sprintf(szName, "x%i", m_typeTwoRequire);
		m_typeTwoRequireLabel->setString(szName);
		m_typeTwoRequireLabel->setPosition(posLabel2);
		m_typeTwoRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeTwoRequireLabel->setVisible(true);
		m_pTypeTwo->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeTwo,kForeground);

		//type 7
		m_typeSevenRequire = 20;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel3);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		m_gameTime = 570.0f;
		break;
	case 19://20 type 06, 20 type 07, 10 type 08
		//type 6
		m_typeSixRequire = 20;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		m_typeSixRequireLabel->setPosition(posLabel1);
		m_typeSixRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSixRequireLabel->setVisible(true);
		m_pTypeSix->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeSix,kForeground);

		//type 7
		m_typeSevenRequire = 20;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel2);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		//type 8
		m_typeEightRequire = 10;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 600.0f;
		break;
	case 20://25 type 07, 20 type 08
		//type 7
		m_typeSevenRequire = 15;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel1);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		//type 8
		m_typeEightRequire = 20;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel2);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 630.0f;
		break;
	case 21://60 type 02, 50 type 03, 20 type 08
		//type 2
		m_typeTwoRequire = 60;
		sprintf(szName, "x%i", m_typeTwoRequire);
		m_typeTwoRequireLabel->setString(szName);
		m_typeTwoRequireLabel->setPosition(posLabel1);
		m_typeTwoRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeTwoRequireLabel->setVisible(true);
		m_pTypeTwo->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeTwo,kForeground);

		//type 3
		m_typeThreeRequire = 50;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		m_typeThreeRequireLabel->setPosition(posLabel2);
		m_typeThreeRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeThreeRequireLabel->setVisible(true);
		m_pTypeThree->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeThree,kForeground);

		//type 8
		m_typeEightRequire = 20;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 660.0f;
		break;
	case 22://30 type 08
		//type 8
		m_typeEightRequire = 30;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel1);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 690.0f;
		break;
	case 23://25 type 06, 25 type 07, 15 type 08
		//type 6
		m_typeSixRequire = 25;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		m_typeSixRequireLabel->setPosition(posLabel1);
		m_typeSixRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSixRequireLabel->setVisible(true);
		m_pTypeSix->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeSix,kForeground);

		//type 7
		m_typeSevenRequire = 25;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		m_typeSevenRequireLabel->setPosition(posLabel2);
		m_typeSevenRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeSevenRequireLabel->setVisible(true);
		m_pTypeSeven->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeSeven,kForeground);

		//type 8
		m_typeEightRequire = 15;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel3);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos3);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 710.0f;
		break;
	case 24://40 type 01, 30 type 08
		// type 1
		m_typeOneRequire = 40;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		m_typeOneRequireLabel->setPosition(posLabel1);
		m_typeOneRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeOneRequireLabel->setVisible(true);
		m_pTypeOne->setPosition(pos1);
		m_mainLayer->addChild(m_pTypeOne,kForeground);

		//type 8
		m_typeEightRequire = 30;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		m_typeEightRequireLabel->setPosition(posLabel2);
		m_typeEightRequireLabel->setAnchorPoint(ccp(0,0.5f));
		m_typeEightRequireLabel->setVisible(true);
		m_pTypeEight->setPosition(pos2);
		m_mainLayer->addChild(m_pTypeEight,kForeground);

		m_gameTime = 720.0f;
		break;
	default:
		break;
	}
}

void MainGame::addLevelHandle(kTagCell _type, int _bonusCount)
{
	if(m_modeGame != kModeGameLevel)
		return;
	char szName[20] = {0};
	switch (_type)
	{
	case kTypeCellOne:
		m_typeOneRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeOneRequire);
		m_typeOneRequireLabel->setString(szName);
		break;
	case kTypeCellTwo:
		m_typeTwoRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeTwoRequire);
		m_typeTwoRequireLabel->setString(szName);
		break;
	case kTypeCellThree:
		m_typeThreeRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeThreeRequire);
		m_typeThreeRequireLabel->setString(szName);
		break;
	case kTypeCellFour:
		m_typeFourRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeFourRequire);
		m_typeFourRequireLabel->setString(szName);
		break;
	case kTypeCellFive:
		m_typeFiveRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeFiveRequire);
		m_typeFiveRequireLabel->setString(szName);
		break;
	case kTypeCellSix:
		m_typeSixRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeSixRequire);
		m_typeSixRequireLabel->setString(szName);
		break;
	case kTypeCellSeven:
		m_typeSevenRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeSevenRequire);
		m_typeSevenRequireLabel->setString(szName);
		break;
	case kTypeCellEight:
		m_typeEightRequire -= _bonusCount;
		sprintf(szName, "x%i", m_typeEightRequire);
		m_typeEightRequireLabel->setString(szName);
		break;
	default:
		break;
	}
	levelHandle();
}

void MainGame::levelHandle()
{
	if(m_typeOneRequire <= 0 && m_typeOneRequireLabel->isVisible())
	{
		m_typeOneRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeOneRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}
	if(m_typeTwoRequire <= 0 && m_typeTwoRequireLabel->isVisible())
	{
		m_typeTwoRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeTwoRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}
	if(m_typeThreeRequire <= 0 && m_typeThreeRequireLabel->isVisible())
	{
		m_typeThreeRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeThreeRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}
	if(m_typeFourRequire <= 0 && m_typeFourRequireLabel->isVisible())
	{
		m_typeFourRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeFourRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}
	if(m_typeFiveRequire <= 0 && m_typeFiveRequireLabel->isVisible())
	{
		m_typeFiveRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeFiveRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}
	if(m_typeSixRequire <= 0 && m_typeSixRequireLabel->isVisible())
	{
		m_typeSixRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeSixRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}
	if(m_typeSevenRequire <= 0 && m_typeSevenRequireLabel->isVisible())
	{
		m_typeSevenRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeSevenRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}
	if(m_typeEightRequire <= 0 && m_typeEightRequireLabel->isVisible())
	{
		m_typeEightRequireLabel->setVisible(false);
		CCSprite* m_pStar = CCSprite::create("Star.png");
		m_pStar->setPosition(m_typeEightRequireLabel->getPosition() + ccp(17,0));
		m_mainLayer->addChild(m_pStar,kForeground);
	}

	if(m_typeOneRequire <= 0 
		&& m_typeTwoRequire <= 0
		&& m_typeThreeRequire <= 0
		&& m_typeFourRequire <= 0
		&& m_typeFiveRequire <= 0
		&& m_typeSixRequire <= 0
		&& m_typeSevenRequire <= 0
		&& m_typeEightRequire <= 0)
	{
		setEndGame();
	}
}

void MainGame::nextLevelHandle(CCObject * pSender)
{
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, MainGame::scene(kModeGameLevel,m_level + 1));
	CCDirector::sharedDirector()->replaceScene(newScene);
}
void MainGame::pauseHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	Grid *m_grid = (Grid*)m_mainLayer->getChildByTag(kGameObjectGrid);
	if(m_grid->getStateGame() == kGameStateRunning)
	{
		m_grid->setTouchEnabled(false);
		m_grid->setStateGame(kGameStatePause);
	}
	m_pauseLayer->setVisible(true);
}
void MainGame::menuHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, MenuScene::scene());
	CCDirector::sharedDirector()->replaceScene(newScene);
}
void MainGame::restartHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, MainGame::scene(m_modeGame,m_level));
	CCDirector::sharedDirector()->replaceScene(newScene);
}
void MainGame::resumeHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	Grid *m_grid = (Grid*)m_mainLayer->getChildByTag(kGameObjectGrid);
	if(m_grid->getStateGame() == kGameStatePause)
	{
		m_grid->setTouchEnabled(true);
		m_grid->setStateGame(kGameStateRunning);
	}
	m_pauseLayer->setVisible(false);
}

void MainGame::addTimeHandle(int value)
{
	if(m_modeGame == kModeGameLevel)
		return;
	m_gameTime += value;

	char szName[20] = {0};
	sprintf(szName, "+%is", value);

	CCLabelBMFont * bonusTimeLabel = CCLabelBMFont::create("123456", "Mia_64.fnt");
	bonusTimeLabel->setScale(0.5f);
	bonusTimeLabel->setColor(BONUS_TIME_COLOR);
	bonusTimeLabel->setAnchorPoint(ccp(0.0f,0.5f));
	bonusTimeLabel->setPosition(BONUS_TIME_POS);
	bonusTimeLabel->setString(szName);
	m_mainLayer->addChild(bonusTimeLabel,kForeground);


	CCAction *m_valueAction = CCSequence::create(
		CCMoveBy::create(0.7f,ccp(0,100)),
		CCCallFuncN::create(this, callfuncN_selector(MainGame::removeFromParrent)),
		NULL
		);
	bonusTimeLabel->runAction(m_valueAction);
}

void MainGame::addScoreHandle(int value,CCPoint pos)
{
	char szName[20] = {0};
	if(value > 0)
	{
		sprintf(szName, "+%i", value);
	}
	else
	{
		sprintf(szName, "%i", value);
	}

	CCLabelBMFont * bonusScoreLabel = CCLabelBMFont::create("123456", "Mia_64.fnt");
	bonusScoreLabel->setScale(0.5f);
	bonusScoreLabel->setColor(ccc3(255,255,255));
	bonusScoreLabel->setAnchorPoint(ccp(0.5f,0.5f));
	bonusScoreLabel->setPosition(pos);
	bonusScoreLabel->setString(szName);
	m_mainLayer->addChild(bonusScoreLabel,kForeground);


	CCAction *m_valueAction = CCSequence::create(
		CCMoveBy::create(0.7f,ccp(0,100)),
		CCCallFuncN::create(this, callfuncN_selector(MainGame::removeFromParrent)),
		NULL
		);
	bonusScoreLabel->runAction(m_valueAction);

	m_score += value;
	//char szName[20] = {0};
	sprintf(szName, "%i", m_score);
	scoreLabel->setString(szName);
	if(m_modeGame != kModeGameLevel)
	{
		if(m_score > m_highScore)
		{
			m_highScore = m_score;
			sprintf(szName, "HighScore:%i", m_highScore);
			highScoreLabel->setString(szName);
			if(m_modeGame == kModeGameEasy)
			{
				CCUserDefault::sharedUserDefault()->setIntegerForKey("HighScoreEasy",m_highScore);
				CCUserDefault::sharedUserDefault()->flush();
			}
			if(m_modeGame == kModeGameHard)
			{
				CCUserDefault::sharedUserDefault()->setIntegerForKey("HighScoreHard",m_highScore);
				CCUserDefault::sharedUserDefault()->flush();
			}
		}
	}
}

void MainGame::removeFromParrent(CCNode* pSender)
{
	pSender->removeFromParent();
}

void MainGame::setEndGame()
{
	if(m_modeGame != kModeGameLevel)
		SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_goalcomplete.wav");
	else
		SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_levelcomplete.wav");

	Grid *m_grid = (Grid*)m_mainLayer->getChildByTag(kGameObjectGrid);
	m_grid->setStateGame(kGameStateEnd);
	m_grid->setTouchEnabled(false);

	CCSprite* m_pComplete = CCSprite::create("Complete.png");
	m_pComplete->setPosition(ccp(visibleSize.width / 2,visibleSize.height / 2 - 200));

	CCSequence* seq = CCSequence::create(
		CCMoveBy::create(1.0f,ccp(0,300)),
		CCCallFunc::create(this, callfunc_selector(MainGame::completeEndGame)),
		NULL
		);
	m_pComplete->runAction(seq);
	m_mainLayer->addChild(m_pComplete,kForeground);
}

void MainGame::completeEndGame()
{
	m_mainLayer->setVisible(false);
	m_resultLayer->setVisible(true);

	if(m_modeGame == kModeGameLevel)
	{
		int bonusScore = (int)m_gameTime * BONUSSCORE_PERSECOND;
		int totalScore = bonusScore + m_score;

		char szName[20] = {0};
		sprintf(szName, "%i", m_score);
		scoreLabelResult->setString(szName);

		sprintf(szName, "%i", bonusScore);
		bonusLabelResult->setString(szName);

		sprintf(szName, "%i", totalScore);
		totalLabelResult->setString(szName);
	}
	else
	{
		char szName[20] = {0};
		sprintf(szName, "%i", m_score);
		scoreLabelResult->setString(szName);

		sprintf(szName, "%i", m_highScore);
		highScoreLabelResult->setString(szName);
	}
}