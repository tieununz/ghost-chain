#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H_

#include "SimpleAudioEngine.h"
#include "cocos2d.h"
USING_NS_CC;
using namespace CocosDenshion;

class SoundManager
{
public:
	static SoundManager* getInstance();
	bool isEnableBackground();
	bool isEnableEffect();
	bool isPlayingBackground();
	void setEnableBackground(bool b);
	void setEnableEffect(bool b);

	
	void preloadBackgroundMusic(const char* path);
	void preloadEffect(const char* path);

	void playBackground(const char* path, bool isPlayAgain = false, bool isLoop = true);
	void stopBackground();
	void pauseBackground();
	void resumeBackground();
	
	void playEffect(const char *path,  bool isLoop = false);

	void setVolumeBackground(float _value);
	void setVolumeFX(float _value);
private:
	SoundManager();
	bool m_bEnableBackground;
	bool m_bEnableEffect;
};

#endif /* SOUND_MANAGER_H */
