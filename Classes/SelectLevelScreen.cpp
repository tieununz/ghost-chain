#include "SelectLevelScreen.h"
#include "GameConfig.h"
#include "SoundManager.h"

CCScene* SelectLevelScreen::scene()
{
	CCScene *scene = CCScene::create();
	SelectLevelScreen *layer = SelectLevelScreen::create();
	scene->addChild(layer);
	return scene;
}

SelectLevelScreen::~SelectLevelScreen(void)
{

}

bool SelectLevelScreen::init()
{
	if ( !CCLayer::init() )
	{
		return false;
	}
	this->setTouchEnabled(true);
	scheduleUpdate();
	m_isMove = false;
	visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	CCSprite* pBG = CCSprite::create("BG_Level.png");
	pBG->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	this->addChild(pBG, kBackground);

	m_selectLayer = CCNode::create();
	m_selectLayer->setPosition(ccp(0,0));
	this->addChild(m_selectLayer,kMiddleground);
	CCSprite* pSelectBG = CCSprite::create("BG_Level_Select.png");
	pSelectBG->setPosition(ccp(visibleSize.width/2 + origin.x + visibleSize.width, visibleSize.height/2 + origin.y));
	m_selectLayer->addChild(pSelectBG, kForeground);

	CCMenuItem *level_01_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_02_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_03_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_04_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_05_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_06_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_07_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_08_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_09_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_10_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_11_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_12_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_13_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_14_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_15_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_16_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_17_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_18_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_19_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_20_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_21_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_22_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_23_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );

	CCMenuItem *level_24_Button = CCMenuItemImage::create(
		"Level_UnLocked.png",
		"Level_UnLocked.png",
		this,
		menu_selector(SelectLevelScreen::selectLevel) );
	level_01_Button->setTag(1);
	level_02_Button->setTag(2);
	level_03_Button->setTag(3);
	level_04_Button->setTag(4);
	level_05_Button->setTag(5);
	level_06_Button->setTag(6);
	level_07_Button->setTag(7);
	level_08_Button->setTag(8);
	level_09_Button->setTag(9);
	level_10_Button->setTag(10);
	level_11_Button->setTag(11);
	level_12_Button->setTag(12);
	level_13_Button->setTag(13);
	level_14_Button->setTag(14);
	level_15_Button->setTag(15);
	level_16_Button->setTag(16);
	level_17_Button->setTag(17);
	level_18_Button->setTag(18);
	level_19_Button->setTag(19);
	level_20_Button->setTag(20);
	level_21_Button->setTag(21);
	level_22_Button->setTag(22);
	level_23_Button->setTag(23);
	level_24_Button->setTag(24);

	level_01_Button->setPosition(LEVEL01);
	level_02_Button->setPosition(LEVEL02);
	level_03_Button->setPosition(LEVEL03);
	level_04_Button->setPosition(LEVEL04);
	level_05_Button->setPosition(LEVEL05);
	level_06_Button->setPosition(LEVEL06);
	level_07_Button->setPosition(LEVEL07);
	level_08_Button->setPosition(LEVEL08);
	level_09_Button->setPosition(LEVEL09);
	level_10_Button->setPosition(LEVEL10);
	level_11_Button->setPosition(LEVEL11);
	level_12_Button->setPosition(LEVEL12);
	level_13_Button->setPosition(LEVEL13);
	level_14_Button->setPosition(LEVEL14);
	level_15_Button->setPosition(LEVEL15);
	level_16_Button->setPosition(LEVEL16);
	level_17_Button->setPosition(LEVEL17);
	level_18_Button->setPosition(LEVEL18);
	level_19_Button->setPosition(LEVEL19);
	level_20_Button->setPosition(LEVEL20);
	level_21_Button->setPosition(LEVEL21);
	level_22_Button->setPosition(LEVEL22);
	level_23_Button->setPosition(LEVEL23);
	level_24_Button->setPosition(LEVEL24);
	

	CCMenu* m_pMenu = CCMenu::create(level_01_Button, level_02_Button, level_03_Button, level_04_Button, level_05_Button,
		 level_06_Button, level_07_Button, level_08_Button, level_09_Button, level_10_Button,
		  level_11_Button, level_12_Button, level_13_Button, level_14_Button, level_15_Button,
		   level_16_Button, level_17_Button, level_18_Button, level_19_Button, level_20_Button,
		    level_21_Button, level_22_Button, level_23_Button, level_24_Button, NULL);
	
	m_pMenu->setPosition(ccp(0, 0));
	m_selectLayer->addChild(m_pMenu,kForeground);

	CCMenuItem *menuButton = CCMenuItemImage::create(
		"Menu_Button_End.png",
		"Menu_Button_End.png",
		this,
		menu_selector(SelectLevelScreen::menuHandle) );
	menuButton->setPosition(visibleSize.width/2,visibleSize.height/2);
	CCMenu* m_pMenu1 = CCMenu::create(menuButton, NULL);
	m_pMenu1->setPosition(ccp(0, 0));
	this->addChild(m_pMenu1,kForeground);

	return true;
}

void SelectLevelScreen::menuHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, MenuScene::scene());
	CCDirector::sharedDirector()->replaceScene(newScene);
}

void SelectLevelScreen::selectLevel(CCObject * pSender)
{
	int level = ((CCNode*)pSender)->getTag();
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, MainGame::scene(kModeGameLevel,level));
	CCDirector::sharedDirector()->replaceScene(newScene);
}


void SelectLevelScreen::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
	CCSetIterator i;
	CCTouch* touch;
	CCPoint tap;

	for( i = pTouches->begin(); i != pTouches->end(); i++)
	{
		touch = (CCTouch*) (*i);

		if(touch && !m_isMove)
		{
			CCPoint diff = touch->getDelta();
			
			int size = 20 * visibleSize.width / 480;
			if((diff.x) >= 10 * FactorX|| (diff.x) <= 10 * FactorX)
			{
				CCPoint addPoint;
				if(diff.x > 0)
				{
					addPoint = ccp(visibleSize.width, 0);
				}
				else
				{
					CCLOG("DIFF.X %f", diff.x);
					addPoint = ccp(-visibleSize.width, 0);
				}
				m_isMove = true;
				CCSequence* seq = CCSequence::create(
					CCMoveBy::create(0.5f,addPoint),
					CCCallFunc::create(this, callfunc_selector(SelectLevelScreen::moveDone)),
					NULL
					);
				m_selectLayer->runAction(seq);
			}
		}
	}
}

void SelectLevelScreen::update(float dt)
{
	if(m_selectLayer->getPositionX() > 0)
		m_selectLayer->setPositionX(0);
	if(m_selectLayer->getPositionX() < - visibleSize.width * 2)
		m_selectLayer->setPositionX(- visibleSize.width * 2);
}

void SelectLevelScreen::moveDone()
{
	m_isMove = false;
}