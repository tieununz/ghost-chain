#include "MenuScene.h"
#include "GameConfig.h"

#include "SoundManager.h"

CCScene* MenuScene::scene()
{
	CCScene *scene = CCScene::create();
	MenuScene *layer = MenuScene::create();
	scene->addChild(layer);
	return scene;
}

MenuScene::~MenuScene(void)
{

}

bool MenuScene::init()
{
	if ( !CCLayer::init() )
	{
		return false;
	}
	
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	CCSprite* pBG = CCSprite::create("BG_Menu.png");
	pBG->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	this->addChild(pBG, kBackground);


	CCMenuItem *playButton = CCMenuItemImage::create(
		"Start_Button.png",
		"Start_Button.png",
		this,
		menu_selector(MenuScene::playButtonHandle) );

	CCMenuItem *aboutButton = CCMenuItemImage::create(
		"About_Button.png",
		"About_Button.png",
		this,
		menu_selector(MenuScene::aboutButtonHandle) );

	playButton->setPosition(ccp(219,visibleSize.height - 486 - FactorY));
	aboutButton->setPosition(ccp(330,visibleSize.height - 590 - FactorY));

	m_pMenu = CCMenu::create(playButton,aboutButton, NULL);
	m_pMenu->setPosition(ccp(0, 0));
	this->addChild(m_pMenu, kForeground);

	return true;
}

void MenuScene::playButtonHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, ModeScene::scene());
	CCDirector::sharedDirector()->replaceScene(newScene);
}

void MenuScene::aboutButtonHandle(CCObject * pSender)
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_button.wav");
	CCScene* newScene = CCTransitionMoveInR::create(0.2f, AboutScene::scene());
	CCDirector::sharedDirector()->replaceScene(newScene);
}