#ifndef __MODE_SCENE_H__
#define __MODE_SCENE_H__
#include "cocos2d.h"
#include "MainGameScene.h"
#include "SelectLevelScreen.h"
#include "GameConfig.h"
USING_NS_CC;

class ModeScene : public CCLayer
{
	CCMenu* m_pMenu;
	void easyButtonHandle(CCObject * pSender);
	void normalButtonHandle(CCObject * pSender);
	void levelButtonHandle(CCObject * pSender);
	void howToPlayButtonHandle(CCObject * pSender);
public:
	~ModeScene(void);
	virtual bool init();
	static CCScene* scene();
	CREATE_FUNC(ModeScene);
};

#endif // __MODE_SCENE_H__