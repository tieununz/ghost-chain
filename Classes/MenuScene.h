#ifndef __MENU_SCENE_H__
#define __MENU_SCENE_H__
#include "cocos2d.h"
#include "GameConfig.h"
#include "ModeScene.h"
#include "AboutScene.h"
USING_NS_CC;

class MenuScene : public CCLayer
{
	CCMenu* m_pMenu;
	void aboutButtonHandle(CCObject * pSender);
	void playButtonHandle(CCObject * pSender);
public:
	~MenuScene(void);
	virtual bool init();
	static CCScene* scene();
	CREATE_FUNC(MenuScene);
};

#endif // __MENU_SCENE_H__