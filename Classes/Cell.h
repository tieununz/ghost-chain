//
//  Cell.h
//
//
//  Created by Tieunun on 21/11/2013
//

#ifndef __CELL_H__
#define __CELL_H__

#include "cocos2d.h"
#include "Grid.h"
#include "GameConfig.h"
USING_NS_CC;

class Grid;

class Cell : public CCSprite
{
public:
	CC_SYNTHESIZE(Grid *, m_gridGame, GridGame);
	CC_SYNTHESIZE(int, m_indexI, IndexI);
	CC_SYNTHESIZE(int, m_indexJ, IndexJ);
	CC_SYNTHESIZE(bool, m_isTouch, IsTouch);
	CC_SYNTHESIZE(int, m_type, Type);
	
	int m_tempIndexI;
	int m_tempIndexJ;

	~Cell(void);
	Cell(Grid *_grid, int _indexI, int _indexJ,int _type);
	static Cell * create(Grid *_grid, int _indexI, int _indexJ,int _type);
	bool canTouching(int _indexI, int _indexJ);
	void changeTouching();
	void resetCell();

	void moveEndTouching(int _lastIndexI,int _lastIndexJ);
	void moveBy(CCPoint desPoint,int _indexI,int _indexJ,float duration); //= Fall by
	void playSound();
private:
	void initCell();
	void setIndex();


	//virtual void setPosition(const CCPoint& pos);
};
#endif // __CELL_H__