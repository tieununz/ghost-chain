//
//  Cell.cpp
//
//
//  Created by Tieunun on 21/11/2013
//
//

#include "Cell.h"
#include "SoundManager.h"
using namespace CocosDenshion;
Cell::~Cell(void)
{
}

Cell::Cell(Grid *_grid, int _indexI, int _indexJ,int _type)
{
	m_gridGame = _grid;
	m_indexI = _indexI;
	m_indexJ = _indexJ;
	m_type = _type;
}

Cell * Cell::create(Grid *_grid, int _indexI, int _indexJ,int _type)
{
	Cell *sprite = new Cell(_grid,_indexI,_indexJ,_type);
	char *pszFileName;
	switch (_type)
	{
		case kTypeCellOne:
			pszFileName ="Type01_NoTouch_1.png";
			break;
		case kTypeCellTwo:
			pszFileName ="Type02_NoTouch_1.png";
			break;
		case kTypeCellThree:
			pszFileName ="Type03_NoTouch_1.png";
			break;
		case kTypeCellFour:
			pszFileName ="Type04_NoTouch_1.png";
			break;
		case kTypeCellFive:
			pszFileName ="Type05_NoTouch_1.png";
			break;
		case kTypeCellSix:
			pszFileName ="Type06_NoTouch_1.png";
			break;
		case kTypeCellSeven:
			pszFileName ="Type07_NoTouch_1.png";
			break;
		case kTypeCellEight:
			pszFileName ="Type08_NoTouch_1.png";
			break;
		default:
			break;
	}
	if(sprite && sprite->initWithFile(pszFileName))
	{
		sprite->initCell();
		sprite->autorelease();
		float width = sprite->getContentSize().width;
		float height = sprite->getContentSize().height;
		
		sprite->setPosition(ccp(width/2 + _indexJ * width, height/2 + _indexI * height) + EXTRAPOS);
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Cell::initCell()
{

}

void Cell::resetCell()
{
	char *pszFileName;
	switch (m_type)
	{
	case kTypeCellOne:
		pszFileName ="Type01_NoTouch_1.png";
		break;
	case kTypeCellTwo:
		pszFileName ="Type02_NoTouch_1.png";
		break;
	case kTypeCellThree:
		pszFileName ="Type03_NoTouch_1.png";
		break;
	case kTypeCellFour:
		pszFileName ="Type04_NoTouch_1.png";
		break;
	case kTypeCellFive:
		pszFileName ="Type05_NoTouch_1.png";
		break;
	case kTypeCellSix:
		pszFileName ="Type06_NoTouch_1.png";
		break;
	case kTypeCellSeven:
		pszFileName ="Type07_NoTouch_1.png";
		break;
	case kTypeCellEight:
		pszFileName ="Type08_NoTouch_1.png";
		break;
	default:
		break;
	}
	this->initWithFile(pszFileName);
}

void Cell::changeTouching()
{
	char *pszFileName;
	
	switch (m_type)
	{
	case kTypeCellOne:
		pszFileName ="Type01_Touching_1.png";
		break;
	case kTypeCellTwo:
		pszFileName ="Type02_Touching_1.png";
		break;
	case kTypeCellThree:
		pszFileName ="Type03_Touching_1.png";
		break;
	case kTypeCellFour:
		pszFileName ="Type04_Touching_1.png";
		break;
	case kTypeCellFive:
		pszFileName ="Type05_Touching_1.png";
		break;
	case kTypeCellSix:
		pszFileName ="Type06_Touching_1.png";
		break;
	case kTypeCellSeven:
		pszFileName ="Type07_Touching_1.png";
		break;
	case kTypeCellEight:
		pszFileName ="Type08_Touching_1.png";
		break;
	default:
		break;
	}
	this->initWithFile(pszFileName);
}

void Cell::moveEndTouching(int _lastIndexI,int _lastIndexJ)
{
	if(m_indexI == _lastIndexI && m_indexJ == _lastIndexJ)
	{
		this->runAction(CCScaleTo::create(0.01f,0.01f,0.01f));
	}
	else
	{
		CCPoint desPoint = ccp(CELLSIZE/2 + _lastIndexJ * CELLSIZE, CELLSIZE/2 + _lastIndexI * CELLSIZE) + EXTRAPOS;
		int bonusI = abs(m_indexI - _lastIndexI);
		int bonusJ = abs(m_indexJ - _lastIndexJ);
		float bonus = (float)((bonusI + bonusJ)/ 30.0f);
		this->runAction(CCScaleTo::create(0.4f,0.01f,0.01f));
		this->runAction(CCSequence::create(CCMoveTo::create(0.15f + bonus,desPoint),CCCallFunc::create(this, callfunc_selector(Cell::playSound)),NULL));
		this->setZOrder(kCellgroundEnd);
	}
}

void Cell::playSound()
{
	SoundManager::getInstance()->playEffect("Sounds/Effects/sfx_join2.wav");
}

bool Cell::canTouching(int _indexI, int _indexJ)
{
	int tempI,tempJ;

	tempI = _indexI + 1;
	tempJ = _indexJ;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;

	tempI = _indexI - 1;
	tempJ = _indexJ;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;

	tempI = _indexI;
	tempJ = _indexJ + 1;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;
	
	tempI = _indexI;
	tempJ = _indexJ - 1;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;

	tempI = _indexI - 1;
	tempJ = _indexJ - 1;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;

	tempI = _indexI + 1;
	tempJ = _indexJ + 1;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;

	tempI = _indexI + 1;
	tempJ = _indexJ - 1;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;

	tempI = _indexI - 1;
	tempJ = _indexJ + 1;
	if (m_indexI == tempI && m_indexJ == tempJ)
		return true;

	return false;
}

void Cell::moveBy(CCPoint desPoint,int _indexI,int _indexJ,float duration)
{
	m_tempIndexI = _indexI;
	m_tempIndexJ = _indexJ;

	CCSequence* seq = CCSequence::create(
		CCMoveTo::create(duration,desPoint),
		CCCallFunc::create(this, callfunc_selector(Cell::setIndex)),
		NULL
		);
	this->runAction(seq);
}
void Cell::setIndex()
{
	m_indexI = m_tempIndexI;
	m_indexJ = m_tempIndexJ;
	if(m_gridGame->getStateGame() == kGameStateRunning)
		m_gridGame->setTouchEnabled(true);
}