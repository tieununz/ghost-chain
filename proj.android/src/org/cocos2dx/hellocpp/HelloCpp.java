/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.hellocpp;



import com.facebook.*;
import com.facebook.model.*;
import com.facebook.widget.*;
import com.facebook.widget.WebDialog.OnCompleteListener;

import android.content.Context;
import android.content.Intent;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import com.easyndk.classes.AndroidNDKHelper;
public class HelloCpp extends Cocos2dxActivity{
	private UiLifecycleHelper uiHelper;
	String CPPFunctionToBeCalled = null;
	
    protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);	
		AndroidNDKHelper.SetNDKReciever(this);
		
		// Try to get your hash key
        try {
		    PackageInfo info = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_SIGNATURES);
		    for (Signature signature : info.signatures) {
		        MessageDigest md = MessageDigest.getInstance("SHA");
		        md.update(signature.toByteArray());
		        Log.v("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		    }
		}
        catch (NameNotFoundException e)
        {
            
		}
        catch (NoSuchAlgorithmException e)
        {
        	
		}
        
        
        uiHelper = new UiLifecycleHelper(this, new Session.StatusCallback() {  
			// callback when session changes state
			@Override
			public void call(Session session, SessionState state, Exception exception) {
			}
		});
        uiHelper.onCreate(savedInstanceState);
      }

    public Cocos2dxGLSurfaceView onCreateView() {
    	Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
    	// HelloCpp should create stencil buffer
    	glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
    	
    	return glSurfaceView;
    }

    public void LoginFacebook(JSONObject prms)
    {
    	try
		{
			CPPFunctionToBeCalled = prms.getString("to_be_called");
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	/*if ( Session.getActiveSession() == null ) {
    		Log.i("LoginAAAAAAA", "LoginAAAAAAAAAAa");
    		Session.getActiveSession().openForRead(new Session.OpenRequest(this).setCallback(new Session.StatusCallback() {
    		    @Override
    		    public void call(Session session, SessionState state, Exception exception) {
    		    	if(exception != null)
    		    		Toast.makeText(getActivity().getApplicationContext(), 
                                "Login Error", 
                                Toast.LENGTH_SHORT).show();
    		    	Log.i("Login111111", "Login111111");
    		    	AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    		    	return;
    		    }
    		}));
	   	}
    	else
    	{*/
    		Session.openActiveSession(this, true, new Session.StatusCallback() {
    		    @Override
    		    public void call(Session session, SessionState state, Exception exception) {
    		    	if(exception != null)
    		    	{
    		    		Toast.makeText(getActivity().getApplicationContext(), 
                                "Login Error", 
                                Toast.LENGTH_SHORT).show();
    		    		Log.i("Login Error", exception.toString());
    		    	}
    		    	AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    		    	return;
    		    }
    		});
    	//}
    }
    
    public void LogoutFacebook(JSONObject prms)
    {
    	String CPPFunctionToBeCalled = null;
		try
		{
			CPPFunctionToBeCalled = prms.getString("to_be_called");
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (Session.getActiveSession() != null) {
			Session.getActiveSession().close();
			Session.getActiveSession().closeAndClearTokenInformation();
		}

		AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    }
    
    @SuppressWarnings("deprecation")
	public void GetInfo(JSONObject prms)
    {
    	if(!isAvailable(prms))
    		return;
    	
		List<String> PERMISSIONS = Arrays.asList("basic_info");
        List<String> permissions = Session.getActiveSession().getPermissions();
        if (!isSubsetOf(PERMISSIONS, permissions)) {
        	Session.NewPermissionsRequest newPermissionsRequest = new Session
    			      .NewPermissionsRequest(this, PERMISSIONS);
              Session.getActiveSession().requestNewPublishPermissions(newPermissionsRequest);
        }
		
		Request.executeMeRequestAsync(Session.getActiveSession(), new Request.GraphUserCallback() {
			// callback after Graph API response with user object
            @Override
            public void onCompleted(GraphUser user, Response response) {
            	if (user != null) {
            		// Generic, ex: network error
                    Toast.makeText(getActivity().getApplicationContext(), 
                        "Get Info Complete", 
                        Toast.LENGTH_SHORT).show();
            		
            		JSONObject userProfile = new JSONObject();
            		try {
            			userProfile.put("facebookId", user.getId());
            			
                        // Populate the JSON object 
                        userProfile.put("facebookId", user.getId());
                        userProfile.put("name", user.getName());
                        if (user.getLocation().getProperty("name") != null) {
                            userProfile.put("location", (String) user
                                    .getLocation().getProperty("name"));    
                        }                           
                        if (user.getProperty("gender") != null) {
                            userProfile.put("gender",       
                                    (String) user.getProperty("gender"));   
                        }                           
                        if (user.getBirthday() != null) {
                            userProfile.put("birthday",     
                                    user.getBirthday());                    
                        }                           
                        if (user.getProperty("relationship_status") != null) {
                            userProfile                     
                                .put("relationship_status",                 
                                    (String) user                                           
                                        .getProperty("relationship_status"));                               
                        }
                        if(user.getProperty("email") != null) {
                        	userProfile                     
                            .put("email",                 
                                (String) user                                           
                                    .getProperty("email")); 
                        }
                        // Now add the data to the UI elements
                        // ...

                    } catch (JSONException e) {
                    	
                    }
            		
            		AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, userProfile);
            	}
            	else
            	{
            		// Generic, ex: network error
                    Toast.makeText(getActivity().getApplicationContext(), 
                        "Get Info Error", 
                        Toast.LENGTH_SHORT).show();
            	}
            	
            }
		});
    }
    
    public void ShareOnwall(JSONObject prms)
    {
    	if(!isAvailable(prms))
    		return;
        Session.NewPermissionsRequest newPermissionsRequest = new Session
        		.NewPermissionsRequest(this, Arrays.asList("publish_actions"));
        Session.getActiveSession().requestNewPublishPermissions(newPermissionsRequest);
        String description = "Get on a spaceship & Protect your family";
        try
		{
        	description = prms.getString("description");
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		if (FacebookDialog.canPresentShareDialog(getApplicationContext(),FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
			Log.i("Shareee", "Shareee");
			// Publish the post using the Share Dialog
			FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this)
			.setName("Reon Neon Game")
			.setCaption("Neon Color World")
			.setDescription(description)
	        .setLink("http://www.facebook.com/")
	        .setPicture("http://i.imgur.com/1yN9PDV.png")
			.build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		} else {
			// Fallback. For example, publish the post using the Feed Dialog
			
			Bundle params = new Bundle();
			params.putString("name", "Reon Neon Game");
			params.putString("message", "Ping Bang Bang!! The aliens are coming");
			params.putString("caption", "Neon Color World");
			params.putString("description", description);
			params.putString("link", "http://www.facebook.com/");
			params.putString("picture", "http://i.imgur.com/1yN9PDV.png");

			WebDialog feedDialog = (
			        new WebDialog.FeedDialogBuilder(HelloCpp.this,
			            Session.getActiveSession(),
			            params))
			        .setOnCompleteListener(new OnCompleteListener() {

            @Override
            public void onComplete(Bundle values,
                FacebookException error) {
                if (error == null) {
                    // When the story is posted, echo the success
                    // and the post Id.
                    final String postId = values.getString("post_id");
                    if (postId != null) {
                        Toast.makeText(getActivity(),
                            "Posted story, id: "+postId,
                            Toast.LENGTH_SHORT).show();
                    } else {
                        // User clicked the Cancel button
                        Toast.makeText(getActivity().getApplicationContext(), 
                            "Publish Cancelled", 
                            Toast.LENGTH_SHORT).show();
                    }
                } else if (error instanceof FacebookOperationCanceledException) {
                    // User clicked the "x" button
                    Toast.makeText(getActivity().getApplicationContext(), 
                        "Publish Cancelled", 
                        Toast.LENGTH_SHORT).show();
                } else {
                    // Generic, ex: network error
                    Toast.makeText(getActivity().getApplicationContext(), 
                        "Share Error", 
                        Toast.LENGTH_SHORT).show();
                }
            }

        })
			        .build();
			    feedDialog.show();

		}
		AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    }
    
    public void InviteFriends(JSONObject prms)
    {
    	if(!isAvailable(prms))
    		return;
		sendRequestDialog();
		AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    }
    
    public void PublishStoryWithData(JSONObject prms)
    {
    	if(!isAvailable(prms))
    		return;
    	Session.NewPermissionsRequest newPermissionsRequest = new Session
        		.NewPermissionsRequest(this, Arrays.asList("publish_actions"));
        Session.getActiveSession().requestNewPublishPermissions(newPermissionsRequest);
		publishStoryWithData(prms);
		AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    }
    
    private void publishStoryWithData(JSONObject prms) {
        if (Session.getActiveSession() != null){
            List<String> PERMISSIONS = Arrays.asList("publish_actions", "publish_stream");
            List<String> permissions = Session.getActiveSession().getPermissions();
            if (!isSubsetOf(PERMISSIONS, permissions)) {
            	Session.NewPermissionsRequest newPermissionsRequest = new Session
        			      .NewPermissionsRequest(this, PERMISSIONS);
                  Session.getActiveSession().requestNewPublishPermissions(newPermissionsRequest);
            }
            String description = "Get on a SPACESHIP & Protect THE EARTH";
            try
    		{
            	description = prms.getString("description");
    		}
    		catch (JSONException e)
    		{
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            
            Bundle postParams = new Bundle();
            postParams.putString("name", "Reon Neon Game");
            postParams.putString("caption", "Neon Color World");
            postParams.putString("message", "Ping Bang Bang!! The ALIENS are coming");
            postParams.putString("description", description);
            postParams.putString("link", "http://www.facebook.com/");
            postParams.putString("picture", "http://i.imgur.com/1yN9PDV.png");
            
            Request.Callback callback= new Request.Callback() {
                public void onCompleted(Response response) {
                    FacebookRequestError error = response.getError();
                    if (error != null) {
                        Toast.makeText(getActivity().getApplicationContext(),
                             "Share Error",
                             Toast.LENGTH_SHORT).show();
                        } else {
                         JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
				        String postId = null;
				        try {
				        	postId = graphResponse.getString("id");
				        } catch (JSONException e) {
				
				        }
                        Toast.makeText(getActivity().getApplicationContext(), postId, Toast.LENGTH_LONG).show();
                    }
                }
            };
            Request request = new Request(Session.getActiveSession(), "me/feed", postParams, 
                                  HttpMethod.POST, callback);

            RequestAsyncTask task = new RequestAsyncTask(request);
            task.execute();
        }
    }
    
    public void PublishStory(JSONObject prms)
    {
    	if(!isAvailable(prms))
    		return;
    	Session.NewPermissionsRequest newPermissionsRequest = new Session
        		.NewPermissionsRequest(this, Arrays.asList("publish_actions"));
        Session.getActiveSession().requestNewPublishPermissions(newPermissionsRequest);
		publishStory();
		AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    }
    
    private void sendRequestDialog() {
        Bundle params = new Bundle();
        params.putString("message", "Ping Bang Bang!! The aliens are coming");
        params.putString("caption", "Neon Color World");
        params.putString("description", "Get on a spaceship & Protect your family");
        params.putString("link", "http://www.facebook.com/");
        params.putString("picture", "http://i.imgur.com/1yN9PDV.png");

        WebDialog requestsDialog = (
            new WebDialog.RequestsDialogBuilder( HelloCpp.this,
                Session.getActiveSession(),
                params))
                .setOnCompleteListener(new OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                        FacebookException error) {
                        if (error != null) {
                            if (error instanceof FacebookOperationCanceledException) {
                                Toast.makeText(getActivity().getApplicationContext(), 
                                    "Request Cancelled", 
                                    Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), 
                                    "Network Error", 
                                    Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            final String requestId = values.getString("request");
                            if (requestId != null) {
                                Toast.makeText(getActivity().getApplicationContext(), 
                                    "Request Sent",  
                                    Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), 
                                    "Request Cancelled", 
                                    Toast.LENGTH_SHORT).show();
                            }
                        }   
                    }

                })
                .build();
        requestsDialog.show();
    }
    
    private void publishStory() {
        if (Session.getActiveSession() != null){
            List<String> PERMISSIONS = Arrays.asList("publish_actions", "publish_stream");
            List<String> permissions = Session.getActiveSession().getPermissions();
            if (!isSubsetOf(PERMISSIONS, permissions)) {
            	Session.NewPermissionsRequest newPermissionsRequest = new Session
        			      .NewPermissionsRequest(this, PERMISSIONS);
                  Session.getActiveSession().requestNewPublishPermissions(newPermissionsRequest);
            }
            
            Bundle postParams = new Bundle();
            postParams.putString("name", "Reon Neon Game");
            postParams.putString("caption", "Neon Color World");
            postParams.putString("message", "Ping Bang Bang!! The aliens are coming");
            postParams.putString("description", "Get on a spaceship & Protect your family");
            postParams.putString("link", "http://www.facebook.com/");
            postParams.putString("picture", "http://i.imgur.com/1yN9PDV.png");
            
            Request.Callback callback= new Request.Callback() {
                public void onCompleted(Response response) {
                    FacebookRequestError error = response.getError();
                    if (error != null) {
                        Toast.makeText(getActivity().getApplicationContext(),
                             "Share Error",
                             Toast.LENGTH_SHORT).show();
                        } else {
                         JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
				        String postId = null;
				        try {
				        	postId = graphResponse.getString("id");
				        } catch (JSONException e) {
				
				        }
                        Toast.makeText(getActivity().getApplicationContext(), postId, Toast.LENGTH_LONG).show();
                    }
                }
            };
            Request request = new Request(Session.getActiveSession(), "me/feed", postParams, 
                                  HttpMethod.POST, callback);

            RequestAsyncTask task = new RequestAsyncTask(request);
            task.execute();
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                //Log.e("Activity", String.format("Error: %s", error.toString()));
            	Toast.makeText(getActivity().getApplicationContext(), 
                        "Share Error", 
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            	Toast.makeText(getActivity().getApplicationContext(), 
            			"Share Completed", 
                        Toast.LENGTH_SHORT).show();
                //Log.i("Activity", "Success!");
            }
        });
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
    
    static {
        System.loadLibrary("cocos2dcpp");
    }
    private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }
    
    private HelloCpp getActivity()
    {
    	return HelloCpp.this;
    }
    
    private boolean isNetworkReachable() {
		boolean bRet = false;
		try {
			ConnectivityManager conn = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = conn.getActiveNetworkInfo();
			bRet = (null == netInfo) ? false : netInfo.isAvailable();
		} catch (Exception e) {
			//LogE("Fail to check network status", e);
		}
		return bRet;
	}
    
    public boolean isLoggedIn() {
        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean isAvailable(JSONObject prms) {
		try
		{
			CPPFunctionToBeCalled = prms.getString("to_be_called");
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	if(!isLoggedIn())
    	{
			Toast.makeText(getActivity().getApplicationContext(), 
                    "Login First", Toast.LENGTH_SHORT).show();
			AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
			return false;
    	}
    	if(!isNetworkReachable())
    	{
    		Toast.makeText(getActivity().getApplicationContext(), 
                    "Network Error", Toast.LENGTH_SHORT).show();
    		AndroidNDKHelper.SendMessageWithParameters(CPPFunctionToBeCalled, null);
    		return false;
    	}
    	
    	return true;
    }
}
